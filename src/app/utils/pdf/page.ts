import { EntetePage } from "./entete_page";
import { PiedPage } from './pied_page';

export class PageFormat
{
    static A1="a1";
    static A2="a2";
    static A3="a3";
    static A4="a4";
    static A5="a5";
    static A6="a6";
    static A7="a7";
    static A8="a8";
    static A9="a9";
    static A10="a10";

    static B1="b1";
    static B2="b2";
    static B3="b3";
    static B4="b4";
    static B5="b5";
    static B6="b6";
    static B7="b7";
    static B8="b8";
    static B9="b9";
    static B10="b10";

    static C1="c1";
    static C2="c2";
    static C3="c3";
    static C4="c4";
    static C5="c5";
    static C6="c6";
    static C7="c7";
    static C8="c8";
    static C9="c9";
    static C10="c10";

    static DL="dl";

    static GOVERNMENT_LETTER="government-letter";

    static LEGAL="legal";

    static JUNION_LEGAR="junion-legar";

    static LEDGER="ledger";

    static TABLOID="tabloid";

    static CREDIT_CARD="credit-card";
}

export class PageOrientation
{
    static PORTRAIT="portrait";
    static LANDSCAPE="landscape";
}

export class Page
{
    orientation:String=PageOrientation.PORTRAIT;
    format:String=PageFormat.A4;

    entete:EntetePage=new EntetePage();
    pied:PiedPage = new PiedPage();

    constructor(format:String=PageFormat.A4,orientation:String=PageOrientation.PORTRAIT)
    {
        this.format=format;
        this.orientation=orientation;
    }

}