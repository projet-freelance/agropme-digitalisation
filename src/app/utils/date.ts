export function getDateNow()
{
    let date=new Date();
    return {
        date:`${date.getDate()}/${(date).getMonth()+1}/${date.getFullYear()}`,
        heures:`${date.getHours()}:${date.getMinutes()}`
      }
}