export class User{
    email:String="";
    id:String="";
    password:String="";
    name:String="";
    phone:String="";
    constructor(email:string="",password:string="")
    {
        this.email = email;
        this.password = password;
    }
    hydrate(obj:any)
    {
        this.email = obj.email;
        this.name = obj.name;
        this.phone=obj.phone;
        this.id=obj.id;
    }
}