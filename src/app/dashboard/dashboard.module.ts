import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker'; 

import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { DashboardModuleRouting } from './dashboard-routing.module';
import { PreloaderComponent } from './shared/preloader/preloader.component';
import { NavBarComponent } from './shared/nav-bar/nav-bar.component';
import { LeftSidebarNavComponent } from './shared/left-sidebar-nav/left-sidebar-nav.component';
import { BreadcrumbsComponent } from './shared/breadcrumbs/breadcrumbs.component';
import { FooterComponent } from './shared/footer/footer.component';
import { FormCvAdminComponent } from './pages/form-cv-admin/form-cv-admin.component';
import { FormGrilleEvaluationCandidaturePersonnesComponent } from './pages/form-grille-evaluation-candidature-personnes/form-grille-evaluation-candidature-personnes.component';
import { FormIdentificationPersonnesRessourceComponent } from './pages/form-identification-personnes-ressource/form-identification-personnes-ressource.component';
import { ProjetComponent } from './pages/projet/projet.component';
import { TimelineModule } from './shared/timeline/timeline.module';
import { WebmailComponent } from './pages/webmail/webmail.component';
import { MailboxModule } from './shared/mailbox/mailbox.module';
import { MailboxInboxComponent } from './pages/mailbox-inbox/mailbox-inbox.component';
import { MailboxReadmailComponent } from './pages/mailbox-readmail/mailbox-readmail.component';
import { MailboxComposeComponent } from './pages/mailbox-compose/mailbox-compose.component';
import { ChatPagesComponent } from './pages/chat-pages/chat-pages.component';
import { ChatModule } from './shared/chat/chat.module';
import { FormFicheRapportMensuelVenteComponent } from './pages/form-fiche-rapport-mensuel-vente/form-fiche-rapport-mensuel-vente.component';
import { FormFicheRapportPeiodiqueExecutionTravauxComponent } from './pages/form-fiche-rapport-peiodique-execution-travaux/form-fiche-rapport-peiodique-execution-travaux.component';
import { FormFicheTravauxRapportPeriodiqueComponent } from './pages/form-fiche-travaux-rapport-periodique/form-fiche-travaux-rapport-periodique.component';
import { FormFicheTravauxSuiviQuotidienComponent } from './pages/form-fiche-travaux-suivi-quotidien/form-fiche-travaux-suivi-quotidien.component';
import { InformationGeneraleDossierComponent } from './pages/information-generale-dossier/information-generale-dossier.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListPersonnelComponent } from './pages/list-personnel/list-personnel.component';
import { ModalBoxModule } from './shared/modal-box/modal-box.module';
import { FlashMessageContainerModule } from './shared/flash-message-container/flash-message-container.module';
import { NewProjetComponent } from './pages/new-projet/new-projet.component';
import { FormulaireProcessusComponent } from './shared/formulaire-processus/formulaire-processus.component';
import { TousCouriersComponent } from './pages/tous-couriers/tous-couriers.component';
import { ProcessHandlerComponent } from './pages/process-handler/process-handler.component';
import { DashboardDirective } from './directives/dahsboardhost.directive';
import { BonCommandeComponent } from './pages/qualification_consultant/bon_commande/bon_commande.component';
import { FormModule } from './shared/form/form.module';
import { TraitementPropositionClientComponent } from './pages/entente_directe/traitement-proposition-client/traitement-proposition-client.component';
import { GestionPersonnelChoisiComponent } from './pages/entente_directe/gestion-personnel-choisi/gestion-personnel-choisi.component';
import { SearchInputComponent } from './shared/search-input/search-input.component';
import { CouriersSortantComponent } from './pages/couriers-sortant/couriers-sortant.component';
import { CouriersEntrantComponent } from './pages/couriers-entrant/couriers-entrant.component';
import { PieceAdminComponent } from './pages/appel_manifestation_interet/piece-admin/piece-admin.component';
import { RepartitionRoleComponent } from './pages/appel_manifestation_interet/repartition-role/repartition-role.component';
import { ConvocationReuinionComponent } from './pages/appel_manifestation_interet/convocation-reuinion/convocation-reuinion.component';
import { FinalisationContratComponent } from './pages/entente_directe/finalisation-contrat/finalisation-contrat.component';
import { NotifiPersonnelComponent } from './pages/entente_directe/notifi-personnel/notifi-personnel.component';
import { DocAdminComponent } from './pages/entente_directe/doc-admin/doc-admin.component';
import { OffreFinanciereComponent } from './pages/entente_directe/offre-financiere/offre-financiere.component';
import { ChronogrammeComponent } from './pages/entente_directe/chronogramme/chronogramme.component';
import { DocPersonnelChoisiComponent } from './pages/entente_directe/doc-personnel-choisi/doc-personnel-choisi.component';

@NgModule({
  declarations: [
    DashboardComponent,
    PreloaderComponent,
    NavBarComponent,
    LeftSidebarNavComponent,
    BreadcrumbsComponent,
    FooterComponent,
    FormCvAdminComponent,
    FormGrilleEvaluationCandidaturePersonnesComponent,
    FormIdentificationPersonnesRessourceComponent,
    ProjetComponent,
    WebmailComponent,
    MailboxInboxComponent,
    MailboxReadmailComponent,
    MailboxComposeComponent,
    ChatPagesComponent, 
    BonCommandeComponent,

    InformationGeneraleDossierComponent,
    FormFicheTravauxSuiviQuotidienComponent,
    FormFicheRapportPeiodiqueExecutionTravauxComponent,
    FormFicheTravauxRapportPeriodiqueComponent,
    FormFicheRapportMensuelVenteComponent,
    ListPersonnelComponent,
    NewProjetComponent,
    FormulaireProcessusComponent,
    TousCouriersComponent,
    CouriersEntrantComponent,
    CouriersSortantComponent,
    ProcessHandlerComponent,
    DashboardDirective,
    TraitementPropositionClientComponent,
    GestionPersonnelChoisiComponent,
    SearchInputComponent,
    DocPersonnelChoisiComponent,
    ChronogrammeComponent,
    DocAdminComponent,
    OffreFinanciereComponent,
    NotifiPersonnelComponent,
    FinalisationContratComponent,
    ConvocationReuinionComponent,
    RepartitionRoleComponent,
    PieceAdminComponent,
  ],
  
  imports: [
    DashboardModuleRouting,
    CommonModule,
    MatStepperModule,
    TimelineModule,
    MailboxModule,
    ChatModule,
    FormsModule,
    ReactiveFormsModule,
    ModalBoxModule,
    FlashMessageContainerModule,
    FormModule,
  ]
})
export class DashboardModule { }
