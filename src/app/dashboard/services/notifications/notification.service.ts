import { Injectable } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { Notification } from './../../models/notification';
import { AuthService } from './../../../auth/services/auth.service';
import { FirebaseApi } from './../../../services/firebase/FirebaseApi';
import { ResultStatut } from './../../../services/firebase/ResultStatut';
import { FirebaseConstant } from './../../../services/firebase/FirebaseConstant';
@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private notifications:Notification[]=[];
  userNotif:any;
  notificationSubject = new Subject<Notification[]>();

  constructor(
      private authService:AuthService,
      private fetchApiService:FirebaseApi
    ) 
    {
      if(!this.authService.isConnected()) return;
      this.userNotif=authService.getUser();
      this.getUnreadNotificationFromAPI()
      .then((result:ResultStatut)=>{
        this.notifications=result.result.map((notif)=>{
          let n = notif[Object.keys(notif)[0]];
          return Notification.fromObject(n);
        });
        this.emitNotification();
      })
   }
   getAllUserNotifications():Notification[]
   {
    let result:Notification[]=[];
    result=this.userNotif.notifications.lues.map((notif)=>Notification.fromObject(notif));
    this.userNotif.notifications.non_lues.forEach(notif => result.push(Notification.fromObject(notif)));
    return result;
  }
  sendNewNotification(notif:Notification,idUser:String):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>{
      this.fetchApiService.set(`users_admin/${idUser}/notifications/non_lues/${notif.id}`,notif.toString())
      .then(result=>{
        resolve(result)})
      .catch(error=>{
        this.fetchApiService.handleApiError(error);
        reject(error);
      })
    });    
  }
  markAsRead(notif:Notification):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>{
      this.fetchApiService.delete(`users_admin/${this.userNotif.id}/notifications/non_lues/${notif.id}`)
      .then((result:ResultStatut)=> this.fetchApiService.set(`users_admin/${this.userNotif.id}/notifications/lues/${notif.id}`,notif.toString()))
      .then((result:ResultStatut)=>{
        let pos=this.userNotif.findIndex((not)=>not.id==notif.id);
        if(pos!==-1) this.notifications.splice(pos,1);
        this.emitNotification();
        resolve(result)
      })
      .catch((error:ResultStatut)=>{
        this.fetchApiService.handleApiError(error);
        reject(error);
      });
    });
  }

  getUnreadNotificationFromAPI():Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>{
      this.fetchApiService.fetch(`users_admin/${this.userNotif.id}/notifications/non_lues/`)
      .then((result:ResultStatut)=> resolve(result))
      .catch((error:ResultStatut)=>{
        this.fetchApiService.handleApiError(error);
        reject(error);
      })
    })
  }

  emitNotification():void
  {
    this.notificationSubject.next(this.notifications.slice());
  }
}
