import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class ExhangeDataService {

  private askDataSource = new Subject<string>();
  private getDataSource = new Subject<any>();

  askData = this.askDataSource.asObservable();
  getData = this.getDataSource.asObservable();

  ask()
  {
    this.askDataSource.next("ask");
  }
  get(data:any)
  {
    this.getDataSource.next(data);
  }

}
