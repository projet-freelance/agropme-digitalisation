import { Injectable } from '@angular/core';
import { CV } from '../../models/cv'
import { ResultStatut } from '../../../services/firebase/ResultStatut';
import { FirebaseApi } from '../../../services/firebase/FirebaseApi';
import { Subject } from 'rxjs';
import { Evaluation } from '../../models/evaluation';

@Injectable({
  providedIn: 'root'
})
export class PersonnelDataService {
  private cvs:CV[]=[];
  private evals:Evaluation[]=[];
  cvsSubject = new Subject<any[]>();
  evalsSubject = new Subject<any[]>();

  constructor(private fetchApi:FirebaseApi) { 
    /*let cvn=new CV();
    cvn.firstName="Cédric";
    cvn.lastName="Nguendap";
    this.cvs.push(cvn);
    let evn=new Evaluation();
    evn.idPersonnel=cvn.idPersonnel;
    this.evals.push(evn);*/

    this.getUsersFromApi()
    .then((result:ResultStatut)=>{
      this.cvs=[];
      this.evals=[];
      for (let key in result.result)
      {
        console.log("result",result.result[key])
        if(result.result[key].cv) this.cvs.push(CV.fromObject(result.result[key].cv));
        if(result.result[key].evaluation)  this.evals.push(Evaluation.fromObject(result.result[key].evaluation));
      }
      result.result=[];
      this.emit("cv");
      this.emit("eval");
    });
   }

  subscribeToCV()
  {
    return this.cvsSubject;
  }
  subscribeToEvaluation()
  {
    return this.evalsSubject;
  }

  addCV(cv:CV):Promise<ResultStatut>
  {
    return this.add(`/users/${cv.idPersonnel}/cv`,cv,"cv");
  }
  updateCV(cv:CV):Promise<ResultStatut>
  {
    return this.update(`/users/${cv.idPersonnel}/cv`,cv,"cv");
  }
  addEvaluation(ev:Evaluation):Promise<ResultStatut>
  {
    return this.add(`/users/${ev.idPersonnel}/evaluation`,ev,"eval")
  }
  updateEvaluation(ev:Evaluation):Promise<ResultStatut>
  {
    return this.update(`/users/${ev.idPersonnel}/evaluation`,ev,"eval");
  }

  protected add(link:String,data:any,type:String):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>
    {
      this.fetchApi.set(link.toString(),data.toString())
      .then((result:ResultStatut)=>{
        let arr:any;
        if(type=="cv") arr=this.cvs;
        else if(type=="eval") arr=this.evals;
        arr.push(data);
        this.emit(type);        
        resolve(result);
      })
      .catch((error:ResultStatut)=>{
        this.fetchApi.handleApiError(error);
        reject(error)
      });
    });
  }
  protected update(link:String,data:any,type:String):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>
    {
      this.fetchApi.set(link.toString(),data.toString())//`/users/${cv.idPersonnel}/cv`
      .then((result:ResultStatut)=>{
        let arr:any;
        if(type=="cv") arr=this.cvs;
        else if(type=="eval") arr=this.evals;

        let pos=arr.findIndex((cve:CV)=>cve.id==data.id);
        if(pos<0) arr.push(data);
        else arr.splice(pos,1);
        this.emit(type);
        resolve(result);
      })
      .catch((error:ResultStatut)=>{
        this.fetchApi.handleApiError(error);
        reject(error)
      });
    })
  }

  getUsersFromApi():Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>{
      this.fetchApi.fetchOnce('users')
      .then((result:ResultStatut)=> resolve(result))
      .catch( (error:ResultStatut)=>{
        this.fetchApi.handleApiError(error);
        reject(error)
      })
    });
  }
  emit(type):void
  {
    if(type=="cv") this.cvsSubject.next(this.cvs.slice());
    else if(type=="eval") this.evalsSubject.next(this.evals.slice());
  }

  findCV(idPersonnel:string):CV
  {
    let cv=this.cvs.find((cvF:CV)=>cvF.idPersonnel==idPersonnel);
    if(cv==undefined) return null;
    return cv;
  }
  getCVList():CV[]
  {
    return this.cvs;
  }

  findEvaluation(idPersonnel:string):Evaluation
  {
    let evaluation=this.evals.find((ev:Evaluation)=>ev.idPersonnel==idPersonnel);
    if(evaluation==undefined) return null;
    return evaluation;
  }
}
