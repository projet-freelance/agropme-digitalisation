import { Injectable } from '@angular/core';
import jsPDF from 'jspdf';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import { CV } from '../../models/cv';
import { PDF } from './../../../utils/pdf/pdf';
@Injectable({
  providedIn: 'root'
})
export class PdfGeneratorService {

  constructor() { }
  genratePdfFromHTML(content,name):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>{
      let pdf=new jsPDF('p','pt');
      let margins ={
        top:80,
        bottom: 60,
        left:57,
        right:57
      }
      pdf.html(content,
        {
          callback:(doc)=> {
            doc.save(name)
            resolve(new ResultStatut())
          }
        })
    })
    

  }
}
