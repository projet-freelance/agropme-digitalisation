import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { FirebaseApi } from 'src/app/services/firebase/FirebaseApi';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import { getDateNow } from 'src/app/utils/date';
import { Client } from '../../metiers/entites/client';
import { ProcessusSelectionParEntenteDirect } from '../../metiers/selection_par_entente_directe.proc';
import { ProcessusSelectionParQualificationConsultant } from '../../metiers/selection_par_qualification_consultant.poc';
import { ProcessusAppelManifestationInteret } from '../../metiers/appel_manifestation_interet.proc';

import { Activite } from '../../models/activite';
import { Processus } from './../../models/processus';
import  { ProcessusType } from './../../models/processustype';


@Injectable({
  providedIn: 'root'
})
export class MoteurProcessusService {
  protected proccessList:Map<String,Processus>=new Map();
  processListSubject:any=new Subject<any[]>();

  constructor(private fetchApi:FirebaseApi) {
    this.fetchApi.fetch("/projets")
    .then((result:ResultStatut)=>{
      for(let prok in result.result)
      {
        console.log("result ", ProcessusType.SELECTION_PAR_ENTENTE_DIRECT.id,result.result[prok]);
        if(result.result[prok].type.id==ProcessusType.SELECTION_PAR_ENTENTE_DIRECT.id)
        {
          this.proccessList.set(result.result[prok].id,ProcessusSelectionParEntenteDirect.fromObject(result.result[prok]));
        }
        else if(result.result[prok].type.id==ProcessusType.SELECTION_PAR_QUALIFICATION.id)
        {
          this.proccessList.set(result.result[prok].id,ProcessusSelectionParQualificationConsultant.fromObject(result.result[prok]));
        }
        else if(result.result[prok].type.id==ProcessusType.APPEL_MANIFESTATION_INTERET.id)
        {
          this.proccessList.set(result.result[prok].id,ProcessusAppelManifestationInteret.fromObject(result.result[prok]));
        }
      }
      //console.log("Processlist ",this.proccessList)
      this.emit();
    });
    /*let npt=new ProcessusSelectionParQualificationConsultant(
      "test",
      "qdlmjqdf",
      new Client(),"","");
      npt.id="2";
    this.proccessList.set("2",npt);
      */
  }
  subscribeToProcess()
  {
    return this.processListSubject;
  }

  emit():void
  {
    this.processListSubject.next(this.getProcessList());
  }
  newProcess(processType:String,title:String,description:String,client:Client,dateDebut,dateFin):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>
    {
      let proc:Processus;
      if(processType==ProcessusType.SELECTION_PAR_ENTENTE_DIRECT.id)
      {
        proc=new ProcessusSelectionParEntenteDirect(title,description,client,dateDebut,dateFin);
      }
      else if(processType==ProcessusType.SELECTION_PAR_QUALIFICATION.id)
      {
        proc = new ProcessusSelectionParQualificationConsultant(title,description,client,dateDebut,dateFin);
      }
      else if(processType==ProcessusType.APPEL_MANIFESTATION_INTERET.id)
      {
        proc = new ProcessusAppelManifestationInteret(title,description,client,dateDebut,dateFin);
      }
      else
      {
        let errorStatut:ResultStatut=new ResultStatut();
        errorStatut.code=ResultStatut.UNKNOW_ERROR;
        errorStatut.message="Type de proccessus inconnu";
        reject(errorStatut);
      }
      

      this.fetchApi.set(`/projets/${proc.id}`,proc.toString())
      .then((result:ResultStatut)=>{
        this.proccessList.set(proc.id,proc);
        result.result=proc;
        this.emit()
        resolve(result);
      })
      .catch((error:ResultStatut)=>reject(error));
    });
  }
  nextProcessStep(processId:String,data:any):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>
    {
      let result:ResultStatut=new ResultStatut();
      if(processId==undefined || processId==null)
      {
        result.apiCode=ResultStatut.INVALID_ARGUMENT_ERROR;
        result.message=`L'identifiant du processus ne peut être vide`;
        reject(result);
      }
      if(!this.proccessList.has(processId))
      {
        result.apiCode=ResultStatut.RESSOURCE_NOT_FOUND_ERROR;
        result.message=`Processus d'indentification ${processId} est introuvable`;
        reject(result);
      }

      let currActivity = this.proccessList.get(processId).getCurrentActivity();
      currActivity.data=data;
      currActivity.dateExecution=getDateNow()
      result.result=this.proccessList.get(processId).nextActivity();
      this.fetchApi.set(`/projets/${processId}`,this.proccessList.get(processId).toString())
      .then((r:ResultStatut)=>{
        resolve(result);
      })
      .catch((error:ResultStatut)=>{
        this.fetchApi.handleApiError(error);
        reject(error)
      });
      resolve(result);
      
    });
  }

  getProcess(processId):Processus
  {
    if(processId==undefined || !this.existProcess(processId)) return null;
    return this.proccessList.get(processId);
  }
  existProcess(processId:String):boolean
  {
    if(processId==undefined) return false;
    return this.proccessList.has(processId.toString());
  }
  getProcessList()
  {
    let procL=[];
    for(let poc of this.proccessList.keys())
    {
      procL.push(this.proccessList.get(poc));
    }
    return procL;
  }
}
