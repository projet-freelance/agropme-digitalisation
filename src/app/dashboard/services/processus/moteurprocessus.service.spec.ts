import { TestBed } from '@angular/core/testing';

import { MoteurProcessusService } from './moteurprocessus.service';

describe('MoteurprocessusService', () => {
  let service: MoteurprocessusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoteurprocessusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
