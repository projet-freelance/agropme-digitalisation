import { Injectable } from '@angular/core';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import { CourierEntrant, CourierSortant } from './../../models/courier';
import { FirebaseApi } from './../../../services/firebase/FirebaseApi';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CourierService {
  courierEntrant: CourierEntrant[] = [];
  courierSortant: CourierSortant[] = [];
  entrantSubject = new Subject<any[]>();
  sortantSubject = new Subject<any[]>();

  constructor(private fetchApi: FirebaseApi) {

    this.getListCourierEntrantFromAPI()
      .then((result: ResultStatut) => {
        this.courierEntrant = [];
        for (let key in result.result) {
          // console.log("result", result.result[key])
          if (result.result[key]) this.courierEntrant.push(CourierEntrant.fromObject(result.result[key]));
        }
        result.result = [];
        // console.log(this.courierEntrant);
        this.emit("courierEntrant");
      });

    this.getListCourierSortantFromAPI()
      .then((result: ResultStatut) => {
        this.courierSortant = [];
        for (let key in result.result) {
          // console.log("result", result.result[key])
          if (result.result[key]) this.courierSortant.push(CourierSortant.fromObject(result.result[key]));
        }
        result.result = [];
        // console.log(this.courierSortant)
        this.emit("courierSortant");
      });

  }

  saveCourierEntrant(ce: CourierEntrant): Promise<ResultStatut> {
    return new Promise<ResultStatut>((resolve, reject) => {
      this.fetchApi.set(`/couriers/in/${ce.id}/`, ce.toString())
        .then((result: ResultStatut) => {
          this.courierEntrant.push(ce);
          this.emit("courierEntrant");
          resolve(result)
        })
        .catch((error: ResultStatut) => {
          this.fetchApi.handleApiError(error)
          reject(error)
        });
    });
  }

  saveCourierSortant(cs: CourierSortant): Promise<ResultStatut> {
    return new Promise<ResultStatut>((resolve, reject) => {
      this.fetchApi.set(`/couriers/out/${cs.id}/`, cs.toString())
        .then((result: ResultStatut) => {
          this.courierSortant.push(cs);
          this.emit("courierEntrant");
          resolve(result)
        })
        .catch((error: ResultStatut) => {
          this.fetchApi.handleApiError(error)
          reject(error)
        });
    });
  }

  getListCourierEntrantFromAPI(): Promise<ResultStatut> {
    return new Promise<ResultStatut>((resolve, reject) => {
      this.fetchApi.fetch('/couriers/in/')
        .then((result: ResultStatut) => resolve(result))
        .catch((error: ResultStatut) => reject(error));
    });
  }

  getListCourierSortantFromAPI(): Promise<ResultStatut> {
    return new Promise<ResultStatut>((resolve, reject) => {
      this.fetchApi.fetch('/couriers/out/')
        .then((result: ResultStatut) => resolve(result))
        .catch((error: ResultStatut) => reject(error));
    });
  }

  emit(type): void {
    if (type == "courierSortant") this.sortantSubject.next(this.courierSortant.slice());
    else if (type == "courierEntrant") this.entrantSubject.next(this.courierEntrant.slice());
  }

  subscribeCourierEntrant() {
    return this.entrantSubject;
  }

  subscribeCourierSortant() {
    return this.sortantSubject;
  }

}
