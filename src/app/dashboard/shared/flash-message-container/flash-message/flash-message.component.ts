import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-flash-message',
  templateUrl: './flash-message.component.html',
  styleUrls: ['./flash-message.component.css']
})
export class FlashMessageComponent implements OnInit {
  @Input() content:string;
  @Input() type:string='info';
  @Input() duration:number=5000;
  @Input() show:boolean=false;
  @Input() styles;
  @Output() eventChange=new EventEmitter<Object>();
  @Input() temporairyFlash:boolean=true;
  @ViewChild('flashMessage') flashMessage:ElementRef;
  constructor() { }

  ngOnInit(): void {
    if(this.temporairyFlash) setTimeout(()=> this.hideMessage(),this.duration);
  }
  ngOnChanges(changes:SimpleChanges):void
  {
    if(changes.show!==undefined && changes.show.currentValue)
    {
      this.ngOnInit();
    }
  }
  hideMessage()
  {
    this.flashMessage.nativeElement.style.display="none";
    this.eventChange.emit({show:false});
  }
  showMessage()
  {
     this.flashMessage.nativeElement.style.display="block";
     this.ngOnInit();
  }

}
