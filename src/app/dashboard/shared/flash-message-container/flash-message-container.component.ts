import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-flash-message-container',
  templateUrl: './flash-message-container.component.html',
  styleUrls: ['./flash-message-container.component.css']
})
export class FlashMessageContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
