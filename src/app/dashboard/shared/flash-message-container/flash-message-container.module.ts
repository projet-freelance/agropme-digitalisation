import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlashMessageContainerComponent } from './flash-message-container.component';
import { FlashMessageComponent } from './flash-message/flash-message.component';



@NgModule({
  declarations: [
    FlashMessageContainerComponent,
    FlashMessageComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    FlashMessageContainerComponent,
    FlashMessageComponent
  ]
})
export class FlashMessageContainerModule { }
