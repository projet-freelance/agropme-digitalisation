import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireProcessusComponent } from './formulaire-processus.component';

describe('FormulaireProcessusComponent', () => {
  let component: FormulaireProcessusComponent;
  let fixture: ComponentFixture<FormulaireProcessusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireProcessusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireProcessusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
