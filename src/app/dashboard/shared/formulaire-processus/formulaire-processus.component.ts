import { Component, OnInit } from '@angular/core';
import { MoteurProcessusService } from './../../services/processus/moteurprocessus.service';

@Component({
  selector: 'app-formulaire-processus',
  templateUrl: './formulaire-processus.component.html',
  styleUrls: ['./formulaire-processus.component.css']
})
export class FormulaireProcessusComponent implements OnInit {

  titlePage:String="Execution du processus: ";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Processus'
    },
    {
      'link':'#',
      'text':'Execution',
    },
  ]


  constructor(private procMotor:MoteurProcessusService) { }

  ngOnInit(): void {
  }

}
