import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerPartComponent } from './container-part/container-part.component';
import { BtnPartComponent } from './btn-part/btn-part.component';
import { InboxComponent } from './inbox/inbox.component';
import { ReadComponent } from './read/read.component';
import { ComposeComponent } from './compose/compose.component';
import { MailboxComponent } from './mailbox.component';
import { FolderPartItemComponent } from "./folder-part-item/folder-part-item.component";
import { LabelPartItemComponent } from "./label-part-item/label-part-item.component";
import { InboxMailComponent } from './inbox/inbox-mail/inbox-mail.component';

@NgModule({
  declarations: [
    ContainerPartComponent, 
    FolderPartItemComponent,
    LabelPartItemComponent,
    BtnPartComponent,
    InboxComponent,
    ReadComponent,
    ComposeComponent,
    MailboxComponent,
    InboxMailComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ContainerPartComponent, 
    FolderPartItemComponent,
    LabelPartItemComponent,
    BtnPartComponent,
    InboxComponent,
    ReadComponent,
    ComposeComponent,
    MailboxComponent
  ]
})
export class MailboxModule { }
