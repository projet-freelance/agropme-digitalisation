import { Component, OnInit } from '@angular/core';

declare var $:any;

@Component({
  selector: 'app-webmail-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.css']
})
export class ComposeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $("#compose-textarea").wysihtml5();
  }

}
