import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-mailbox',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './mailbox.component.html',
  styleUrls: ['./mailbox.component.css']
})
export class MailboxComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
