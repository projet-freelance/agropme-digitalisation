import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-inbox-mail',
  templateUrl: './inbox-mail.component.html',
  styleUrls: ['./inbox-mail.component.css']
})
export class InboxMailComponent implements OnInit {
  @Input()sender:String="Unknow";
  @Input()title:String="Title";
  @Input()content:String="";
  @Input()hasAttachment:Boolean=false;
  @Input()date:String="now";
  @Input() link:String="#";
  constructor() { }

  ngOnInit(): void {
  }

}
