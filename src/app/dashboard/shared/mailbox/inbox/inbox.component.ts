import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-webmail-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  @Input()title:String="Inbox";
  constructor() { }

  ngOnInit(): void {
  }

}
