import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderPartItemComponent } from './folder-part-item.component';

describe('FolderPartItemComponent', () => {
  let component: FolderPartItemComponent;
  let fixture: ComponentFixture<FolderPartItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderPartItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderPartItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
