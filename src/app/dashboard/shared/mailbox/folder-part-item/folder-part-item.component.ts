import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-folder-part-item',
  templateUrl: './folder-part-item.component.html',
  styleUrls: ['./folder-part-item.component.css']
})
export class FolderPartItemComponent implements OnInit {
  @Input()label:String="";
  @Input()link:String="#";
  @Input()fa_icon:String="";
  @Input()etiquette:String="";
  @Input()etiquetteClass:String="";
  @Input()classCss:String="";
  @Input()cssStyle:String="";
  constructor() { }

  ngOnInit(): void {
  }

}
