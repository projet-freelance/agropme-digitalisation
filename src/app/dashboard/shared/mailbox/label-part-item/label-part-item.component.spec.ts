import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabelPartItemComponent } from './label-part-item.component';

describe('LabelPartItemComponent', () => {
  let component: LabelPartItemComponent;
  let fixture: ComponentFixture<LabelPartItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabelPartItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabelPartItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
