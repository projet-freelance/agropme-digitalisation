import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-label-part-item',
  templateUrl: './label-part-item.component.html',
  styleUrls: ['./label-part-item.component.css']
})
export class LabelPartItemComponent implements OnInit {
  @Input()label:String="";
  @Input()styleCss:String="";
  @Input()link:String="#";
  @Input()fa_color:String="";
  constructor() { }

  ngOnInit(): void {
  }

}
