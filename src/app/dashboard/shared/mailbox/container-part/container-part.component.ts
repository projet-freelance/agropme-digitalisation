import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-container-part',
  templateUrl: './container-part.component.html',
  styleUrls: ['./container-part.component.css']
})
export class ContainerPartComponent implements OnInit {
  @Input()title:String="Title";
  constructor() { }

  ngOnInit(): void {
  }

}
