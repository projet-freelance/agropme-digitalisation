import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerPartComponent } from './container-part.component';

describe('ContainerPartComponent', () => {
  let component: ContainerPartComponent;
  let fixture: ComponentFixture<ContainerPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
