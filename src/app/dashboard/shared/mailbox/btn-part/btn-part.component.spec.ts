import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnPartComponent } from './btn-part.component';

describe('BtnPartComponent', () => {
  let component: BtnPartComponent;
  let fixture: ComponentFixture<BtnPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
