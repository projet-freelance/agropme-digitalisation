import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-btn-part',
  templateUrl: './btn-part.component.html',
  styleUrls: ['./btn-part.component.css']
})
export class BtnPartComponent implements OnInit {

  @Input()label:String=" ";
  constructor() { }

  ngOnInit(): void {
  }

}
