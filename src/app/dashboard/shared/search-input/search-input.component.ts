import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent implements OnInit {
  @Output() keyupEvent=new EventEmitter<Object>();
  @Input() placeholder="Rechercher personnel";
  input: FormControl=new FormControl('');
  constructor() { }

  ngOnInit(): void {
  }
  submit(data)
  {
    this.keyupEvent.emit(this.input.value);
  }
}
