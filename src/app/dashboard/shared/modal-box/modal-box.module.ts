import { NgModule,TemplateRef  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalBoxComponent } from './modal-box.component';
import { ModalBoxHeaderComponent } from './modal-box-header/modal-box-header.component';
import { ModalBoxFooterComponent } from './modal-box-footer/modal-box-footer.component';
import { ModalBoxBodyComponent } from './modal-box-body/modal-box-body.component';



@NgModule({
  declarations: [
    ModalBoxComponent,
    ModalBoxHeaderComponent,
    ModalBoxFooterComponent,
    ModalBoxBodyComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ModalBoxComponent,
    ModalBoxHeaderComponent,
    ModalBoxFooterComponent,
    ModalBoxBodyComponent

  ]
})
export class ModalBoxModule { }
