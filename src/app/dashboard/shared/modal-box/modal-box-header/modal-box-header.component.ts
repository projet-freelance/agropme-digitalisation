import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-modal-box-header',
  templateUrl: './modal-box-header.component.html',
  styleUrls: ['./modal-box-header.component.css']
})
export class ModalBoxHeaderComponent implements OnInit {
  @Input() modalTitle;
  @Input() textcolor;
  @Input() bgcolor;
  @Output() eventClose=new EventEmitter<Object>();
  constructor() { }

  ngOnInit(): void {
  }
  close()
  {
    this.eventClose.emit();
  }
}
