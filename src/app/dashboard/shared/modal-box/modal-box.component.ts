import { Component, OnInit,HostListener, ViewChild, Input, Output, EventEmitter, ElementRef} from '@angular/core';

@Component({
  selector: 'app-modal-box',
  templateUrl: './modal-box.component.html',
  styleUrls: ['./modal-box.component.css']
})
export class ModalBoxComponent implements OnInit
{
  @Input() show:boolean;
  @Input() headerbackground:string;
  @Input() headerforeground:string;
  @Input() title:string;
  @Input() width:string;
  @Input() showOkBtn:boolean;
  @Input() showNoBtn:boolean;
  @Input() OkBtnText:string="Valider";
  @Input() NoOkBtnText:string="Annuler";
  @Output() eventClose=new EventEmitter<Object>();
  @Output() eventAcceptEvent=new EventEmitter<Object>();
  @Output() eventRefuseEvent=new EventEmitter<Object>();
  @ViewChild('refModalBox') refModalBox:ElementRef;

  constructor() { }

  @HostListener('window:click',['$event.target'])
  onWindowClickHandle(target)
  {
    if(target==this.refModalBox.nativeElement) this.close();
  }
  ngOnInit(): void {
    // console.log(this.viewContainer);
  }
  close():void
  {
    this.eventClose.emit();
  }
  acceptEvent()
  {
    //this.close();
    this.eventAcceptEvent.emit();
  }
  refuseEvent()
  {
    this.close();
    this.eventRefuseEvent.emit();
  }

}
