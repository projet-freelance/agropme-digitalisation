import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-modal-box-footer',
  templateUrl: './modal-box-footer.component.html',
  styleUrls: ['./modal-box-footer.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class ModalBoxFooterComponent implements OnInit {
  @Input() background:string;
  constructor() { }

  ngOnInit(): void {
  }

}
