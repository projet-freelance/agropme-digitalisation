import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../services/notifications/notification.service';
import  { Notification } from './../../models/notification';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  notifications:Notification[]=[];
  constructor(private notificationService:NotificationService) { }

  ngOnInit(): void {
    this.notificationService.notificationSubject.subscribe((notif)=>this.notifications=notif);
  }

}
