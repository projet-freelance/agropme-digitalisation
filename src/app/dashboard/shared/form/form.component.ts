import { Component, OnInit, Input, Output,EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Input() childs: any[];
  @Input() textBtn: string="Valider";
  @Output() submitEvent=new EventEmitter<Object>();
  @Input() textError:string="";
  @Input() textWaiting:string="Veuillez patientez...";
  form:FormGroup;
  submitted:boolean=false;
  @Input() waiting:boolean=false;
  constructor() {}
  

  ngOnInit(): void {
    let obj={};
    this.childs.forEach(elt=>{
      elt.forEach((inputElement)=>{
        if(inputElement.validators===undefined) obj[`${inputElement.id}`]=new FormControl('');
        else obj[`${inputElement.id}`]=new FormControl('',[...inputElement.validators]);
      }); 
    });
    this.form=new FormGroup(obj);
  }
  
  submit()
  {
    if(!this.form.valid)
    {
      console.log("Error", this.form);
      this.textError="Formulaire invalide";
      return;
    }
    this.textError="";
    this.submitted=true;
    this.waiting=true;
    this.submitEvent.emit(this.form.value);    
    //this.childs.forEach(elt=>console.log(`${elt.name} : ${this.form.value[elt.name]}`));
  }

}
