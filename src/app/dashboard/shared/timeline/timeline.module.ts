import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineComponent } from './timeline.component';
import { TimelineItemHeaderComponent } from './timeline-item-header/timeline-item-header.component';
import { TimelineItemFooterComponent } from './timeline-item-footer/timeline-item-footer.component';
import { TimelineLabelComponent } from './timeline-label/timeline-label.component';
import { TimelineItemBodyComponent } from './timeline-item-body/timeline-item-body.component';
import { TimelineItemComponent } from './timeline-item/timeline-item.component';


@NgModule({
  declarations: [
    TimelineComponent,
    TimelineItemHeaderComponent,
    TimelineItemFooterComponent,
    TimelineLabelComponent,
    TimelineItemBodyComponent,
    TimelineItemComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TimelineComponent,
    TimelineItemHeaderComponent,
    TimelineItemFooterComponent,
    TimelineLabelComponent,
    TimelineItemBodyComponent,
    TimelineItemComponent,
  ]
})
export class TimelineModule { }
