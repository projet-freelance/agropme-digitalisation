import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline-item',
  templateUrl: './timeline-item.component.html',
  styleUrls: ['./timeline-item.component.css']
})
export class TimelineItemComponent implements OnInit {
  @Input() date:String;
  @Input() m_icon:String;
  @Input() m_icon_color:String="white-text";
  @Input() m_bg_icon_color:String="white";
  constructor() { }

  ngOnInit(): void {
  }

}
