import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline-item-header',
  templateUrl: './timeline-item-header.component.html',
  styleUrls: ['./timeline-item-header.component.css']
})
export class TimelineItemHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
