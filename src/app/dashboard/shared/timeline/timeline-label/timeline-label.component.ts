import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline-label',
  templateUrl: './timeline-label.component.html',
  styleUrls: ['./timeline-label.component.css']
})
export class TimelineLabelComponent implements OnInit {
  @Input() label: String;
  @Input() fg_color: String = "red";
  @Input() bg_color:String ="white-text";
    constructor() { }

ngOnInit(): void {
}

}
