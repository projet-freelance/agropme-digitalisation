import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatComponent } from './chat.component';
import { ListUserComponent } from './list-user/list-user.component';
import { ListMessageComponent } from './list-message/list-message.component';
import { HeaderComponent } from './header/header.component';
import { WriteMessageComponent } from './write-message/write-message.component';


@NgModule({
  declarations: [
    ChatComponent,
    ListUserComponent,
    ListMessageComponent,
    HeaderComponent,
    WriteMessageComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ChatComponent
  ]
})
export class ChatModule { }
