import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormGrilleEvaluationCandidaturePersonnesComponent } from './form-grille-evaluation-candidature-personnes.component';

describe('FormGrilleEvaluationCandidaturePersonnesComponent', () => {
  let component: FormGrilleEvaluationCandidaturePersonnesComponent;
  let fixture: ComponentFixture<FormGrilleEvaluationCandidaturePersonnesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormGrilleEvaluationCandidaturePersonnesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormGrilleEvaluationCandidaturePersonnesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
