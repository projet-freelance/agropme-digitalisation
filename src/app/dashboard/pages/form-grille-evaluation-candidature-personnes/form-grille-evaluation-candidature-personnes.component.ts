import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { EntityID } from 'src/app/models/EntityID';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import { getDateNow } from 'src/app/utils/date';
import { Evaluation } from '../../models/evaluation';
import { PersonnelDataService } from '../../services/forms/personneldata.service';

declare var MStepper:any;

declare var $:any;


@Component({
  selector: 'app-form-grille-evaluation-candidature-personnes',
  templateUrl: './form-grille-evaluation-candidature-personnes.component.html',
  styleUrls: ['./form-grille-evaluation-candidature-personnes.component.css']
})
export class FormGrilleEvaluationCandidaturePersonnesComponent implements OnInit, AfterViewInit{
  titlePage:String="Fiche: Evaluation candidature";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Formulaire'
    },
    {
      'link':'#',
      'text':'Fiche',
    },
    {
      'link':'#',
      'text':'Evaluation candidature',
    },
  ]

  totalForm=0;
  
  findPersonnel:boolean=true;

  form:FormGroup;
  submittedForm:boolean=false;
  waitingForm:boolean=false;
  errorFormMessage:String="";

  messageFlashMessage:String="";
  showFlashMessage:Boolean=false;
  typeFlashMessage:String="";
  durationFlashMessage=5000;

  evaluation:Evaluation=null;

  constructor(
    private dashboard:ElementRef,
    private actRoute:ActivatedRoute,
    private personnalDataService:PersonnelDataService
    ) { }
  ngAfterViewInit(): void {
    //stepper
    let steppers = this.dashboard.nativeElement.querySelectorAll('.stepper');
    steppers.forEach(stepper => new MStepper(stepper, {
      // options
      firstActive: 0 // this is the default
      })
    );
    $('.datepicker').pickadate({
      format:'dd/mm/yyyy',
      defaultDate:new Date()
    });
  
  }

  ngOnInit(): void {   

    this.actRoute.paramMap.subscribe(params => { 
      this.evaluation = this.personnalDataService.findEvaluation(params.get('id_personnel')); 
    });

    if(this.evaluation==null)
    {
      this.findPersonnel=false;
      return;
    }

    
    let evalString=this.evaluation.toString();
    this.evaluation.dateMiseAJour=getDateNow().date;
    let nom_prenom="";
    let cv=this.personnalDataService.findCV(this.evaluation.idPersonnel.toString());
    if(cv!=null) nom_prenom=`${cv.firstName} ${cv.lastName}`;
    this.form=new FormGroup({
      nom_prenom:new FormControl(nom_prenom),
      devaluateur:new FormControl(evalString.devaluateur,[Validators.required]),
      datemiseajour:new FormControl('',[Validators.required]),
      formation_acad: new FormControl(evalString.formation.academique,[Validators.required,Validators.max(10)]),
      formation_reput: new FormControl(evalString.formation.reputation,[Validators.required]),
      formation_compet: new FormControl(this.evaluation.totalCompetence(),[Validators.required]),
      compet_tra_real: new FormControl(evalString.competence.traveauRealise,[Validators.required]),
      compet_reput_client: new FormControl(evalString.competence.reputationClient,[Validators.required]),
      compet_exp_prat: new FormControl(evalString.competence.experiencePratique,[Validators.required]),
      compet_exp_forma : new FormControl(evalString.competence.experienceFormateur,[Validators.required]),
      compet_exp_consult: new FormControl(evalString.competence.experienceConsultant,[Validators.required]),
      dispo_outil_base: new FormControl(this.evaluation.totalDisponibilite(),[Validators.required]),
      dispo_vehi_perso: new FormControl(evalString.disponibilite.vehiculePerso,[Validators.required]),
      dispo_pc: new FormControl(evalString.disponibilite.pc,[Validators.required]),
      dispo_mait_trait_text: new FormControl(evalString.disponibilite.maitriseTraitText,[Validators.required]),
      dispo_telou_fax: new FormControl(evalString.disponibilite.telOuFax,[Validators.required]),
      qual_perso: new FormControl(this.evaluation.totalQualite(),[Validators.required]),
      qual_reput: new FormControl(evalString.qualite.reputation,[Validators.required]),
      qual_comport: new FormControl(evalString.qualite.comportement,[Validators.required]),
      dispo: new FormControl(evalString.dispo,[Validators.required]),
    });
    this.controlForm();

  }

  controlForm()
  {
    this.form.controls.devaluateur.valueChanges.subscribe((data)=>{
      if(data!=null) this.evaluation.devaluateur=data;
    })
    let formsControls=[
      {
        name:"formation_acad",
        max:10,
        form:"formationAcademique"
      },
      {
        name:"formation_reput",
        max:10,
        form:"reputationInstitution"
      },
      {
        name:"compet_tra_real",
        max:50,
        form:"traveauRealise"
      },
      {
        name:"compet_reput_client",
        max:50,
        form:"reputationClient"
      },
      {
        name:"compet_exp_prat",
        max:50,
        form:"experiencePratique"
      },
      {
        name:"compet_exp_forma",
        max:50,
        form:"experienceFormateur"
      },
      {
        name:"compet_exp_consult",
        max:50,
        form:"experienceConsultant"
      },
      {
        name:"dispo_vehi_perso",
        max:25,
        form:"vehiculePerso"
      },
      {
        name:"dispo_pc",
        max:25,
        form:"pc"
      },
      {
        name:"dispo_mait_trait_text",
        max:25,
        form:"maitriseTraitText"
      },
      {
        name:"dispo_telou_fax",
        max:25,
        form:"telOuFax"
      },
      {
        name:"qual_reput",
        max:10,
        form:"reputation"
      },      
      {
        name:"qual_comport",
        max:10,
        form:"comportement"
      },
      {
        name:"dispo",
        max:5,
        form:"dispo"
      },
    ];
    formsControls.forEach((formElement)=>{
      this.form.controls[formElement.name].valueChanges.subscribe((data)=>{
        if(data == null ) this.form.controls[formElement.name].setValue(0);
        if(data<0) this.form.controls[formElement.name].setValue(0);
        else if(data > formElement.max) this.form.controls[formElement.name].setValue(formElement.max);
        this.evaluation[formElement.form] =this.form.controls[formElement.name].value;
      })
    })
    this.form.controls.formation_acad.valueChanges.subscribe((data)=>
    {
      
    });
  }

  submit()
  {
    if( !this.form.valid )
    {
      this.waitingForm=false;
      this.errorFormMessage="Formulaire invalide";
      return;
    }
    this.waitingForm = true;

    /*let evaluation=Evaluation.fromObject({
      dateMiseAJour:new Date(),
      devaluateur:this.form.value.devaluateur,
      id:"",
      idPersonnel:"",
      formation:{
        academique:this.form.value.formation_acad,
        reputation:this.form.value.formation_reput
      },
      competence:{
        traveauRealise:this.form.value.compet_tra_real,
        reputationClient:this.form.value.compet_reput_client,
        experiencePratique:this.form.value.compet_exp_prat,
        experienceFormateur:this.form.value.compet_exp_forma,
        experienceConsultant:this.form.value.compet_exp_consult
      },
      disponibilite:{
        vehiculePerso:this.form.value.dispo_vehi_perso,
        pc:this.form.value.dispo_pc,
        maitriseTraitText:this.form.value.dispo_mait_trait_text,
        telOuFax:this.form.value.dispo_telou_fax
      },
      qualite:{
        personnel:this.form.value.qual_perso,
        reputation:this.form.value.qual_reput,
        comportement:this.form.value.qual_comport
      },
      dispo:this.form.value.dispo
    });*/

    this.personnalDataService.addEvaluation(this.evaluation)
    .then((result:ResultStatut)=>{
      this.messageFlashMessage="Grille d'évaluation mise a jour avec success";
      this.showFlashMessage=true;
      this.typeFlashMessage="success";
      this.waitingForm = false;
    })
    .catch((error:ResultStatut)=>{
      this.messageFlashMessage="Erreur l'ors de la mise a jour de la grille d'évaluation";
      this.waitingForm = false;
      this.typeFlashMessage = "danger";
      this.errorFormMessage="Echec de l'opération";
    })
  }

}
