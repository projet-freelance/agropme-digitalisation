import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TousCouriersComponent } from './tous-couriers.component';

describe('TousCouriersComponent', () => {
  let component: TousCouriersComponent;
  let fixture: ComponentFixture<TousCouriersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TousCouriersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TousCouriersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
