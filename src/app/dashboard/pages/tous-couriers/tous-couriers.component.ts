import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {CourierEntrant,CourierSortant } from './../../models/courier';

@Component({
  selector: 'app-tous-couriers',
  templateUrl: './tous-couriers.component.html',
  styleUrls: ['./tous-couriers.component.css']
})
export class TousCouriersComponent implements OnInit {
  titlePage:String="Liste des couriers";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Couriers'
    },
    {
      'link':'#',
      'text':'Tous'
    }
  ];

  courierFindInput:FormControl;

  currListCourier:CourierEntrant[] | CourierSortant[] =[];
  constructor() { }

  ngOnInit(): void {
    this.courierFindInput=new FormControl();
  }
  findCourier(data)
  {

  }

}
