import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mailbox-inbox',
  templateUrl: './mailbox-inbox.component.html',
  styleUrls: ['./mailbox-inbox.component.css']
})
export class MailboxInboxComponent implements OnInit {
  titlePage:String="Boite de reception";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Boite mail'
    },
    {
      'link':'#',
      'text':'Boite de reception',
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
