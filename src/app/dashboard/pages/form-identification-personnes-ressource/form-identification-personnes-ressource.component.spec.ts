import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormIdentificationPersonnesRessourceComponent } from './form-identification-personnes-ressource.component';

describe('FormIdentificationPersonnesRessourceComponent', () => {
  let component: FormIdentificationPersonnesRessourceComponent;
  let fixture: ComponentFixture<FormIdentificationPersonnesRessourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormIdentificationPersonnesRessourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormIdentificationPersonnesRessourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
