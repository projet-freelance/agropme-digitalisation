import { Component,ElementRef, OnInit } from '@angular/core';

declare var MStepper:any;

@Component({
  selector: 'app-form-identification-personnes-ressource',
  templateUrl: './form-identification-personnes-ressource.component.html',
  styleUrls: ['./form-identification-personnes-ressource.component.css']
})
export class FormIdentificationPersonnesRessourceComponent implements OnInit {

  titlePage:String="Fiche: Evaluation candidature";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Formulaire'
    },
    {
      'link':'#',
      'text':'Fiche',
    },
    {
      'link':'#',
      'text':'Identification ressource personne',
    },
  ];

  constructor(private dashboard:ElementRef) { }

  ngOnInit(): void {
    //stepper
    let steppers = this.dashboard.nativeElement.querySelectorAll('.stepper');
    steppers.forEach(stepper => new MStepper(stepper, {
      // options
      firstActive: 0 // this is the default
      })
    );
  }

}
