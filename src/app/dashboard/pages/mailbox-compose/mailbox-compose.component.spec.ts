import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailboxComposeComponent } from './mailbox-compose.component';

describe('MailboxComposeComponent', () => {
  let component: MailboxComposeComponent;
  let fixture: ComponentFixture<MailboxComposeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailboxComposeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailboxComposeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
