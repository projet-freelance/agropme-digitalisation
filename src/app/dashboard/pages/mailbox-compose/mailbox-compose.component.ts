import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mailbox-compose',
  templateUrl: './mailbox-compose.component.html',
  styleUrls: ['./mailbox-compose.component.css']
})
export class MailboxComposeComponent implements OnInit {
  titlePage:String="Redaction d'un nouveau message";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Boite mail'
    },
    {
      'link':'#',
      'text':'Nouveau message',
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
