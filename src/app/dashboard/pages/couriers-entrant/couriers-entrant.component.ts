import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import {CourierEntrant } from './../../models/courier';
import { CourierService } from './../../services/courier/courier.service';

declare var $:any;
@Component({
  selector: 'app-couriers-entrant',
  templateUrl: './couriers-entrant.component.html',
  styleUrls: ['./couriers-entrant.component.css']
})
export class CouriersEntrantComponent implements OnInit,  AfterViewInit {
  titlePage:String="Liste des couriers entrant";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Couriers'
    },
    {
      'link':'#',
      'text':'Entrant'
    }
  ];

  courierFindInput:FormControl;

  listeEntrant:CourierEntrant[] = [];
  currListeEntrant:CourierEntrant[] = [];

  formModalArrive:FormGroup;
  submittedModalForm:boolean=false;
  waitingModalForm:boolean=false;
  isConfidentielle=[
    {name:"Confidentiel"},
    {name:"Non confidentiel"}
  ];

  input:FormControl=new FormControl();

  messageFlashMessage:String="";
  showFlashMessage:Boolean=false;
  typeFlashMessage:String="";
  durationFlashMessage=5000;

  errorModalFormMessage:String="";

  modal_box=null;
  constructor( private router:Router,
    private courierEntrant: CourierService) { 

  }

  ngOnInit(): void {
    this.formModalArrive=new FormGroup({
      source:new FormControl('',[Validators.required]),
      dateArrive:new FormControl('',[Validators.required]),
      objet:new FormControl('',[Validators.required]),
      type:new FormControl('',[Validators.required]),
      observation:new FormControl('',[Validators.nullValidator]),
      option:new FormControl('',[Validators.required]),
      serviceDestinataire:new FormControl('',[Validators.required])
    });

    this.courierEntrant.subscribeCourierEntrant().subscribe((courierData)=> {
      this.listeEntrant=courierData;
      this.currListeEntrant=[...this.listeEntrant];
    });
    this.courierEntrant.emit("courierEntrant");
    
  }

  ngAfterViewInit(): void {
    $('.modal').modal({onCloseEnd:()=> this.closeModal()});
    $('#type').material_select();
    $('#type').on('change',()=> this.formModalArrive.controls.type.setValue($('#type').val()));

    $('#serviceDestinataire').material_select();
    $('#serviceDestinataire').on('change',()=> this.formModalArrive.controls.serviceDestinataire.setValue($('#serviceDestinataire').val()));
    
    $('#option').material_select();
    $('#option').on('change',()=> this.formModalArrive.controls.option.setValue($('#option').val()));

    // $('#serviceDestinataire').material_select();
    // $('#serviceDestinataire').on('change',()=> this.formModalArrive.controls.option.setValue($('#serviceDestinataire').val()));

    $('.datepicker_new_courier').pickadate({
      format:'dd/mm/yyyy',
      onSet:(data)=>{
        console.log(data)
        let date = new Date(data.select);
        let sdate=`${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
        this.formModalArrive.controls.dateArrive.setValue(sdate);
      }
    });
  }

  closeModal()
  {
    this.formModalArrive.reset();
    this.waitingModalForm=false;
    this.errorModalFormMessage="";
    $('.modal').modal('close');
    //this.modal_box.close();
  }

  submitModalForm()
  {
    // console.log(this.formModalArrive.value);
    if( !this.formModalArrive.valid )
    {
      this.waitingModalForm=false;
      this.errorModalFormMessage="Formulaire invalide";
      console.log(this.formModalArrive.value);
      return;
    }

    let courierEntrant: CourierEntrant = new CourierEntrant();
    courierEntrant.source=this.formModalArrive.value.source;
    courierEntrant.dateArrive=this.formModalArrive.value.dateArrive;
    courierEntrant.object=this.formModalArrive.value.objet;
    courierEntrant.observation=this.formModalArrive.value.observation;
    courierEntrant.voiTranmission=this.formModalArrive.value.type;
    courierEntrant.serviceDestinataire=this.formModalArrive.value.serviceDestinataire;
    courierEntrant.isConfidentielle=this.formModalArrive.value.option;
    this.waitingModalForm=true;

    this.courierEntrant.saveCourierEntrant(courierEntrant)
    .then((result:ResultStatut)=>
    {
      console.log("saved");
        this.messageFlashMessage="Courier entrant ajouté avec success";
        this.showFlashMessage=true;
        this.typeFlashMessage="success";
        console.log(this.formModalArrive.value);
        this.closeModal();
        this.router.navigate(['/dasboard/couriers/entrant']);
    })
    .catch((error:ResultStatut)=>
    {        
      console.log(error);
      console.log(this.formModalArrive.value);
        this.showFlashMessage=true;
        this.typeFlashMessage="danger";
        this.waitingModalForm=false;
        this.messageFlashMessage=error.message;
        this.errorModalFormMessage=error.message;
    });
  }

  changeFlashMessageStatut(data)
  {
    this.showFlashMessage=data.show;
  }
  findCourier(data){

  }

}
