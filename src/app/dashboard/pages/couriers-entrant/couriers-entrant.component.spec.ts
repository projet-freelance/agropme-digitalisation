import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CouriersEntrantComponent } from './couriers-entrant.component';

describe('CouriersEntrantComponent', () => {
  let component: CouriersEntrantComponent;
  let fixture: ComponentFixture<CouriersEntrantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CouriersEntrantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CouriersEntrantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
