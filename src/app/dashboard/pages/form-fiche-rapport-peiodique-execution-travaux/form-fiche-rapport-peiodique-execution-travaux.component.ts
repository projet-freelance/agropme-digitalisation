import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-fiche-rapport-peiodique-execution-travaux',
  templateUrl: './form-fiche-rapport-peiodique-execution-travaux.component.html',
  styleUrls: ['./form-fiche-rapport-peiodique-execution-travaux.component.css']
})
export class FormFicheRapportPeiodiqueExecutionTravauxComponent implements OnInit {
  titlePage:string = "Fiche: Rapport périodique execution travaux";
  path:any = [
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Formulaire'
    },
    {
      'link':'#',
      'text':'Fiche'
    },
    {
      'link':'#',
      'text':'Rapport périodique execution travaux'
    },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
