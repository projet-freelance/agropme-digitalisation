import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFicheRapportPeiodiqueExecutionTravauxComponent } from './form-fiche-rapport-peiodique-execution-travaux.component';

describe('FormFicheRapportPeiodiqueExecutionTravauxComponent', () => {
  let component: FormFicheRapportPeiodiqueExecutionTravauxComponent;
  let fixture: ComponentFixture<FormFicheRapportPeiodiqueExecutionTravauxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFicheRapportPeiodiqueExecutionTravauxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFicheRapportPeiodiqueExecutionTravauxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
