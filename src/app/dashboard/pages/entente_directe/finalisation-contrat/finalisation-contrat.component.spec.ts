import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalisationContratComponent } from './finalisation-contrat.component';

describe('FinalisationContratComponent', () => {
  let component: FinalisationContratComponent;
  let fixture: ComponentFixture<FinalisationContratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalisationContratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalisationContratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
