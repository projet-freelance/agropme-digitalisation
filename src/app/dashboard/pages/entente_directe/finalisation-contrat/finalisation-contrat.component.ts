import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-finalisation-contrat',
  templateUrl: './finalisation-contrat.component.html',
  styleUrls: ['./finalisation-contrat.component.css']
})
export class FinalisationContratComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  @Input() processId="";

  listUser=[];

  formInput=
  [
    [
      {
        id:"contrat",
        html:"input",
        type:"text",
        text:'Contrat: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud"
        //validators:[Validators.required]       
      },
      {
        id:"avisImposition",
        html:"input",
        type:"text",
        text:'Avid d\'imposition: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud"
        //validators:[Validators.required]       
      },
      
    ],
    [
      {
        id:"ordreVirement",
        html:"input",
        type:"text",
        text:'Ordre de virement : Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud"
        //validators:[Validators.required]       
      },
      {
        id:"transmissionOrdreVirement",
        html:"input",
        type:"checkbox",
        text:'Transmission de l\'ordre de virement a la banque',
        container_class:"col s6", 
        //validators:[Validators.required]       
      },
    ],
    [
      {
        id:"examplaireContrat",
        html:"input",
        type:"checkbox",
        text:'Déposer 7 examplaire du contrat signé et l\'attestation de virement bancaire aupres du FISC',
        container_class:"col s12", 
        //validators:[Validators.required]       
      },
      
    ],
    [
      {
        id:"appositionMensionObligatoire",
        html:"input",
        type:"checkbox",
        text:'Apposer les mentions obligatoire sur les contrats',
        container_class:"col s6", 
        //validators:[Validators.required]       
      },
      {
        id:"retournerCopiSigne",
        html:"input",
        type:"checkbox",
        text:'Retourner le contrat signé au client ',
        container_class:"col s6", 
        //validators:[Validators.required]       
      },
    ]
  ]
  constructor( ) { }

  ngOnInit(): void {
  }
  submitForm(data)
  {
    this.waitingForm=true;
    this.eventSubmit.emit({
      doc:{
        contrat:data.contrat,
        avisImposition: data.avisImposition,
        ordreVirement:data.ordreVirement,
      },
      transmission:{
        ordreVirement:data.transmissionOrdreVirement,
        contratSigner:data.retournerCopiSigne,
        examplaireContrat:data.examplaireContrat
      },
      depot:{
        mentions:data.appositionMensionObligatoire,
      }
    });
  }
}
