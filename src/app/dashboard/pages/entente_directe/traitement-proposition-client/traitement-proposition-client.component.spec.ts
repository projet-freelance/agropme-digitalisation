import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraitementPropositionClientComponent } from './traitement-proposition-client.component';

describe('TraitementPropositionClientComponent', () => {
  let component: TraitementPropositionClientComponent;
  let fixture: ComponentFixture<TraitementPropositionClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraitementPropositionClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraitementPropositionClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
