import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-traitement-proposition-client',
  templateUrl: './traitement-proposition-client.component.html',
  styleUrls: ['./traitement-proposition-client.component.css']
})
export class TraitementPropositionClientComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;

  formInput=
  [
    [
      {
        id:"accuseReception",
        html:"input",
        type:"checkbox",
        text:'Accuse reception de la demande de proposition',
        container_class:"col s6", 
        //validators:[Validators.required]       
      },
      {
        id:"transLettreIntention",
        html:"input",
        type:"checkbox",
        text:'Transmission de la lettre d\'intention de soumission',
        container_class:"col s6",
        input_class:"filled-in", 
        //validators:[Validators.required]       
      },
    ],
    [
      {
        id:"objetReuinion",
        html:"input",
        type:"text",
        text:'Objet de la reuinion d\'analyse de la grille de notation de l\'offre technique',
        container_class:"col s6", 
        icon:"border_color"
        //validators:[Validators.required,Validators.maxLength(6)]       
      },
      {
        id:"dateReuinion",
        html:"input",
        type:"text",
        text:'Date de la reuinion',
        container_class:"col s6",
        input_class:"datepicker", 
        icon:"event"
        //validators:[Validators.required]       
      },
    ]
  ]
  constructor() { }

  ngOnInit(): void {
  }
  submitForm(data)
  {
    this.eventSubmit.emit({
      accuseReception:data.accuseReception,
      transLettreIntention:data.transLettreIntention,
      reuinion:{
        date:data.dateReuinion,
        objet:data.objetReuinion
      }
    });
  }
}
