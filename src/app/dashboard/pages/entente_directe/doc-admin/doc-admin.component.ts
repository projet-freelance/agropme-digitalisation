import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-doc-admin',
  templateUrl: './doc-admin.component.html',
  styleUrls: ['./doc-admin.component.css']
})
export class DocAdminComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  @Input() processId="";

  formInput=
  [
    [
      {
        id:"liendocumentfiscaux",
        html:"input",
        type:"text",
        text:'Docuements fiscal: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
      {
        id:"legalDocJuridique",
        html:"input",
        type:"checkbox",
        text:'Légaliser les documents juridiques',
        container_class:"col s6",
        //validators:[Validators.required]       
      },
    ],
    [
      {
        id:"justificatifCapaciteFinancier",
        html:"input",
        type:"text",
        text:'Justificatif de la capacité financiere: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
    ]
  ]
  constructor() { }

  ngOnInit(): void {
  }
  submitForm(data)
  {
    this.waitingForm=true;
    this.eventSubmit.emit({
      doc:{
        justificatifCapaciteFinancier:data.justificatifCapaciteFinancier,
        docFiscaux:data.liendocumentfiscaux
      },
      legaliser:data.legalDocJuridique
    });
  }


}
