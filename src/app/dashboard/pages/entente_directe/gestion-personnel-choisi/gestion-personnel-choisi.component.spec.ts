import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionPersonnelChoisiComponent } from './gestion-personnel-choisi.component';

describe('GestionPersonnelChoisiComponent', () => {
  let component: GestionPersonnelChoisiComponent;
  let fixture: ComponentFixture<GestionPersonnelChoisiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionPersonnelChoisiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionPersonnelChoisiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
