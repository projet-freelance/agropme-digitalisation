import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CV } from 'src/app/dashboard/models/cv';
import { PersonnelDataService } from 'src/app/dashboard/services/forms/personneldata.service';

@Component({
  selector: 'app-gestion-personnel-choisi',
  templateUrl: './gestion-personnel-choisi.component.html',
  styleUrls: ['./gestion-personnel-choisi.component.css']
})
export class GestionPersonnelChoisiComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  shoulDisabledSubmitButton=true;
  personnelChoisi={};

  cvPersonnel:CV[]=[];
  currCVPersonnel:CV[]=[];
  loading=true;
  constructor(private personnalDataService:PersonnelDataService,) { }

  ngOnInit(): void {
    this.personnalDataService.subscribeToCV().subscribe((userData)=> {
      this.loading = false;
      this.cvPersonnel=userData;
      this.currCVPersonnel=[...this.cvPersonnel];
    });
    this.personnalDataService.emit("cv");
    
  }
  findPersonnel(value)
  {
    if(value.length<1)
    {
      this.currCVPersonnel=[...this.cvPersonnel];
      return;
    }
    this.currCVPersonnel=this.cvPersonnel.filter(obj=>{
      let name=`${obj.firstName} ${obj.lastName}`;
      let pos=name.toLowerCase().indexOf(value.toLowerCase());
      if(pos!==-1) return true;
      return false; 
    });    
  }
  choisiPersonnel(idPersonnel)
  {
    if(this.personnelChoisi.hasOwnProperty(idPersonnel)) delete this.personnelChoisi[idPersonnel];
    else {
      let cv:CV=this.personnalDataService.findCV(idPersonnel);
      this.personnelChoisi[idPersonnel]=`${cv.firstName} ${cv.lastName}`;
    }

    this.shoulDisabledSubmitButton=Object.keys(this.personnelChoisi).length==0
  }
  
  validStep()
  {
    let arr=[];
    for(let key in this.personnelChoisi)
    {
      arr.push({
        id:key,
        nom:this.personnelChoisi[key]
      });
    }
    this.waitingForm=true;

    this.eventSubmit.emit(arr);
  }
}
