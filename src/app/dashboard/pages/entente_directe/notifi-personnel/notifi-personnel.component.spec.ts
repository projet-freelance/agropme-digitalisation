import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifiPersonnelComponent } from './notifi-personnel.component';

describe('NotifiPersonnelComponent', () => {
  let component: NotifiPersonnelComponent;
  let fixture: ComponentFixture<NotifiPersonnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifiPersonnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifiPersonnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
