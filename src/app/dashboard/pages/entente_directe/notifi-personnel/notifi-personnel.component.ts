import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MoteurProcessusService } from 'src/app/dashboard/services/processus/moteurprocessus.service';

@Component({
  selector: 'app-notifi-personnel',
  templateUrl: './notifi-personnel.component.html',
  styleUrls: ['./notifi-personnel.component.css']
})
export class NotifiPersonnelComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  @Input() processId="";

  listUser=[];

  formInput=
  [
    [
      {
        id:"notifUser",
        html:"input",
        type:"checkbox",
        text:'Personnel clés notifié',
        container_class:"col s6", 
        //validators:[Validators.required]       
      },
    ],
    [
      {
        id:"dateNotif",
        html:"input",
        type:"text",
        text:'Date de notification des utilisateurs',
        container_class:"col s6", 
        input_class:"datepicker",
        icon:"event"
        //validators:[Validators.required]       
      },
    ]
  ]
  constructor( private moteurPocess:MoteurProcessusService) { }

  ngOnInit(): void {
    this.listUser=this.moteurPocess.getProcess(this.processId).getDataActivity(2);
  }
  submitForm(data)
  {
    this.waitingForm=true;
    this.eventSubmit.emit({
      notification:{
        validNotification:data.notifUser,
        dateNotification:data.dateNotif,
      }
    });
  }
}
