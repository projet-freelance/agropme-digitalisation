import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Validators } from '@angular/forms';
import { Processus } from 'src/app/dashboard/models/processus';
import { MoteurProcessusService } from 'src/app/dashboard/services/processus/moteurprocessus.service';

@Component({
  selector: 'app-doc-personnel-choisi',
  templateUrl: './doc-personnel-choisi.component.html',
  styleUrls: ['./doc-personnel-choisi.component.css']
})
export class DocPersonnelChoisiComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  @Input() processId="";
  currentProcess:Processus=null;
  dataUserSelected:any[]=[];

  formInput=
  [
    [
      {
        id:"receptDiplome",
        html:"input",
        type:"checkbox",
        text:'Collecter et légaliser les diplômes du personnél clé',
        container_class:"col s6",
        //validators:[Validators.required]       
      },
      {
        id:"receptDoc",
        html:"input",
        type:"checkbox",
        text:'Collecter les attestation de disponibilite du personnél clé',
        container_class:"col s6",
        //validators:[Validators.required]       
      },      
    ],
    [
      {
        id:"docMethodologie",
        html:"input",
        type:"text",
        text:'Document de méthodologie: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
      {
        id:"lienAgenda",
        html:"input",
        type:"text",
        text:'Plan de mise en oeuvre des activités: Lien Google Agenda ',
        container_class:"col s6", 
        icon:"event_available" 
        //validators:[Validators.required]       
      },
    ],
  ]
  constructor(private moteurPocess:MoteurProcessusService) { }

  ngOnInit(): void {
    this.currentProcess = this.moteurPocess.getProcess(this.processId);
    this.dataUserSelected = this.currentProcess.getDataActivity(2);
    //console.log(this.currentProcess,dataUserSelected);

  }

  submitForm(data)
  {
    this.waitingForm=true;
    this.eventSubmit.emit({
      doc:{
        diplome:data.receptDiplome,
        methodologie:data.docMethodologie,
        disponibilite:data.receptDoc
      },
      agenda:data.lienAgenda
    });
  }

}
