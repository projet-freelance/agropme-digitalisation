import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocPersonnelChoisiComponent } from './doc-personnel-choisi.component';

describe('DocPersonnelChoisiComponent', () => {
  let component: DocPersonnelChoisiComponent;
  let fixture: ComponentFixture<DocPersonnelChoisiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocPersonnelChoisiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocPersonnelChoisiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
