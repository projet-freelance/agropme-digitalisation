import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChronogrammeComponent } from './chronogramme.component';

describe('ChronogrammeComponent', () => {
  let component: ChronogrammeComponent;
  let fixture: ComponentFixture<ChronogrammeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChronogrammeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChronogrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
