import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-chronogramme',
  templateUrl: './chronogramme.component.html',
  styleUrls: ['./chronogramme.component.css']
})
export class ChronogrammeComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  @Input() processId="";

  formInput=
  [
    [
      {
        id:"lienAgendaChronogrammeProduction",
        html:"input",
        type:"text",
        text:'Chronogramme de production des livrables: Lien Google Agenda',
        btn_text:'Doc',
        container_class:"col s6", 
        icon:"event_available" 
        //validators:[Validators.required]       
      },
      {
        id:"lienAgendaChronogrammeActivite",
        html:"input",
        type:"text",
        text:'Chronogramme de mise en oeuvre des activités: Lien Google Agenda ',
        container_class:"col s6",
        icon:"event_available" 
        //validators:[Validators.required]       
      },
    ],
  ]
  constructor() { }

  ngOnInit(): void {
  }
  submitForm(data)
  {
    this.waitingForm=true;
    this.eventSubmit.emit({
      chronogramme:{
        miseEnOeuvreActivite:data.lienAgendaChronogrammeActivite,
        productionLivrable:data.lienAgendaChronogrammeProduction
      }
    });
  }

}
