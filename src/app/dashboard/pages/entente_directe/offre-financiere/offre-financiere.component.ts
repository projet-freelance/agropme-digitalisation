import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-offre-financiere',
  templateUrl: './offre-financiere.component.html',
  styleUrls: ['./offre-financiere.component.css']
})
export class OffreFinanciereComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  @Input() processId="";

  formInput=
  [
    [
      {
        id:"offreFinanciere",
        html:"input",
        type:"text",
        text:'Offre financiere: Lien Google Drive',
        btn_text:'Doc',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
      {
        id:"offreTransmise",
        html:"input",
        type:"text",
        text:'Date de transmission de l\'offre financiere',
        container_class:"col s6", 
        input_class:"datepicker",
        icon:"event"
        //validators:[Validators.required]       
      },
    ],
  ]
  constructor() { }

  ngOnInit(): void {
  }
  submitForm(data)
  {
    this.waitingForm=true;
    this.eventSubmit.emit({
      offre:{
        lienDoc:data.offreFinanciere,
        dateTransmission:data.offreTransmise,
      }
    });
  }


}
