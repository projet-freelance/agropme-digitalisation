import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationGeneraleDossierComponent } from './information-generale-dossier.component';

describe('InformationGeneraleDossierComponent', () => {
  let component: InformationGeneraleDossierComponent;
  let fixture: ComponentFixture<InformationGeneraleDossierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationGeneraleDossierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationGeneraleDossierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
