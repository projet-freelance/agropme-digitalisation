import { Component, ElementRef, OnInit } from '@angular/core';

declare var MStepper: any;

@Component({
  selector: 'app-information-generale-dossier',
  templateUrl: './information-generale-dossier.component.html',
  styleUrls: ['./information-generale-dossier.component.css']
})
export class InformationGeneraleDossierComponent implements OnInit {

  titlePage:string = "Information Générale / Dossier";
  path:any = [
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Formulaire'
    },
    {
      'link':'#',
      'text':'Fiche'
    },
    {
      'link':'#',
      'text':'Informations générale dossier'
    },
  ];
  
  constructor(private dashboard:ElementRef) { }

  ngOnInit(): void {
    
    //stepper
    let steppers = this.dashboard.nativeElement.querySelectorAll('.stepper');
    steppers.forEach(stepper => new MStepper(stepper, {
      // options
      firstActive: 0 // this is the default
      })
    );
  }

}
