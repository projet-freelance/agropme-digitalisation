import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCvAdminComponent } from './form-cv-admin.component';

describe('FormCvAdminComponent', () => {
  let component: FormCvAdminComponent;
  let fixture: ComponentFixture<FormCvAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCvAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCvAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
