import { error } from '@angular/compiler/src/util';
import { Component, ElementRef, OnInit, EventEmitter,Output, AfterViewInit, AfterContentChecked, AfterContentInit } from '@angular/core';
import  { FormGroup, FormControl } from '@angular/forms';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import { CV } from '../../models/cv';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonnelDataService } from '../../services/forms/personneldata.service';
import  { PdfGeneratorService } from './../../services/pdf/pdfgenerator.service';

declare var MStepper:any;

declare var DynamicTable:any;

declare var $:any;

@Component({
  selector: 'app-form-cv-admin',
  templateUrl: './form-cv-admin.component.html',
  styleUrls: ['./form-cv-admin.component.css']
})
export class FormCvAdminComponent implements OnInit, AfterViewInit{

  titlePage:String="Formulaire de CV: Modèle administratif";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Formulaire'
    },
    {
      'link':'#',
      'text':'CV',
    },
    {
      'link':'#',
      'text':'modele administratif',
    },
  ];
  findPersonnel:boolean=true;

  form:FormGroup;  
  submittedForm:boolean=false;
  waitingForm:boolean=false;

  waitingModalPDF:boolean=false;

  formationtableInstance:any=null;
  languetableInstance:any=null;
  pubRecherchetableInstance:any=null;
  fonctionActuelletableInstance:any=null;

  etat_civil=[
    {name:"Célibataire"},
    {name:"Marié"}
  ];

  personnel:CV;

  messageFlashMessage:String="";
  showFlashMessage:Boolean=false;
  typeFlashMessage:String="";
  durationFlashMessage=2000;
  
  @Output() submitEvent=new EventEmitter<Object>();

  constructor(
    private dashboard:ElementRef,
    private actRoute:ActivatedRoute,
    private router:Router,
    private personnalDataService:PersonnelDataService,
    private pdfGenerator:PdfGeneratorService
    ) {

      //instantiation des tableaux dynamiques
      this.formationtableInstance = new DynamicTable();
      this.languetableInstance = new DynamicTable();
      this.pubRecherchetableInstance = new DynamicTable();
      this.fonctionActuelletableInstance = new DynamicTable();
    
     }


  ngAfterViewInit(): void {

    if(this.personnel==null) return

    //stepper
    let steppers = this.dashboard.nativeElement.querySelectorAll('.stepper');
    steppers.forEach(stepper => new MStepper(stepper, {
      // options
      firstActive: 0 // this is the default
      })
    );

    //Obtention des references des tableaux dynamiques 
    this.formationtableInstance.setTable(this.dashboard.nativeElement.querySelector('.table_formation'));
    this.languetableInstance.setTable(this.dashboard.nativeElement.querySelector('.table_langue'));
    this.pubRecherchetableInstance.setTable(this.dashboard.nativeElement.querySelector('.table_publication_recherche'));
    this.fonctionActuelletableInstance.setTable(this.dashboard.nativeElement.querySelector('.table_fonction_actuelle'));
    
    setTimeout(() => {
     //mise a jour des valeurs des tableaux dynamiques
    this.personnel.formationAcademique.forEach((fonction)=>this.formationtableInstance.setValue(fonction));
    this.personnel.communication.forEach((com)=>this.languetableInstance.setValue(com));
    this.personnel.publicationRecherche.forEach((pubrech)=>this.pubRecherchetableInstance.setValue(pubrech));
    this.personnel.fonctionActuelle.forEach((foncAct)=>this.fonctionActuelletableInstance.setValue(foncAct));

    });
      
    $('select').material_select();    
    $('select').on('change',()=>this.form.controls.etat_civil.setValue($('select').val().split(' ')[1]))
    $('.datepicker').pickadate({
      format:'dd/mm/yyyy',
      formatSubmit:'dd/mm/yyyy',
      onSet:(data)=>{
        let date=new Date(data.select);
        let sdate=`${date.getDate()}/${(date).getMonth()+1}/${date.getFullYear()}`;
        this.form.controls.dateNaiss.setValue(sdate)
      }
    });

    $('.modal').modal();
    
  }

  ngOnInit(): void {
    this.actRoute.paramMap.subscribe(params => { 
        this.personnel = this.personnalDataService.findCV(params.get('id_personnel')); 
    });

    if(this.personnel==null)
    {
      this.findPersonnel=false;
      return;
    }

    this.form=new FormGroup({
      fname:new FormControl(this.personnel.firstName),
      lname:new FormControl(this.personnel.lastName),
      dateNaiss:new FormControl(this.personnel.dateNaiss),
      nationnalite:new FormControl(this.personnel.nationnalite),
      etat_civil:new FormControl(this.personnel.etatCivil),
      formation_complementaire:new FormControl(this.personnel.formationComplementaire),
      appartOrgaPro:new FormControl(this.personnel.appartOrgaPro),
      autreCompet:new FormControl(this.personnel.autreCompet),
      autres_infos:new FormControl(this.personnel.autreInfosUtils)
    });  
  }
  changeFlashMessageStatut(data)
  {
    this.showFlashMessage=data.show;
  }
  submit()
  {
    this.submittedForm=true;
    this.waitingForm=true;
    this.submitEvent.emit(this.form.value);
    this.personnalDataService
    .addCV(CV.fromObject({
      id:this.personnel.id,
      idPersonnel:this.personnel.idPersonnel,
      personnel:{
        nom:this.form.value.fname,
        prenom:this.form.value.lname,
        dateNaiss:this.form.value.dateNaiss,
        nationnalite:this.form.value.nationnalite,
        etat_civil:this.form.value.etat_civil
      },
      formation:{
          academique:this.formationtableInstance.getValue(),
          complementaire:this.form.value.formation_complementaire,
      },
      communication:this.languetableInstance.getValue(),
      vie_pro:{
          organisme_pro:this.form.value.appartOrgaPro,
          autres_conpet:this.form.value.autreCompet,
          fonction_actuelle:this.fonctionActuelletableInstance.getValue()
      },
      autres_infos:this.form.value.autreCompet,
      pub_recherche:this.pubRecherchetableInstance.getValue()
      }))
      .then((result:ResultStatut)=>
      {
          //valide action
          this.waitingForm=false;
          this.messageFlashMessage="Succes de l'opération de mise a jour du CV";
          this.typeFlashMessage="success";
          this.showFlashMessage=true;
          //redirection vers la page actuelle
          //setTimeout(() => this.router.navigate(['/dashboard/personnels']),2500);
          
      })
      .catch((error:ResultStatut)=>
      {
          //rejecte action
          this.waitingForm=false;
          this.messageFlashMessage="Erreur de l'opération de mise a jour du CV: "+error.message;
          this.showFlashMessage=true;
          this.typeFlashMessage="danger";
          console.log(error);
      })

    //console.log(this.form.value);
    //console.log("Formation ",this.formationtableInstance.getValue());

    //this.childs.forEach(elt=>console.log(`${elt.name} : ${this.form.value[elt.name]}`));
  }

  genereatedPDF()
  {
    this.waitingModalPDF=true;
    this.pdfGenerator.genratePdfFromHTML(this.dashboard.nativeElement.querySelector('#modal_cv_pdf .modal-body'),"CV")
    .then((result:ResultStatut)=>{
        this.waitingModalPDF=false;
        this.messageFlashMessage="Génération du PDF avec success";
        this.typeFlashMessage="success";
        this.showFlashMessage=true;
    });
  }
}

