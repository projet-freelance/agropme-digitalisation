import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Processus } from '../../models/processus';
import { MoteurProcessusService } from '../../services/processus/moteurprocessus.service';

@Component({
  selector: 'app-projet',
  templateUrl: './projet.component.html',
  styleUrls: ['./projet.component.css']
})
export class ProjetComponent implements OnInit {
  titlePage:String="List des projets";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Projet'
    },
    {
      'link':'#',
      'text':'Liste',
    },
  ]

  projetFindInput:FormControl=new FormControl();
  processList:Processus[]=[];
  currProcessList:Processus[]=[];

  constructor(private moteurPocess:MoteurProcessusService) { }

  ngOnInit(): void {
    this.moteurPocess.subscribeToProcess().subscribe((proc)=> this.processList=proc);
    this.currProcessList=[...this.processList];
  }

  findProjet(data)
  {
    let value=this.projetFindInput.value;
    /*if(value.length<1)
    {
      this.currCVPersonnel=[...this.cvPersonnel];
      return;
    }
    this.currCVPersonnel=this.cvPersonnel.filter(obj=>{
      let name=`${obj.firstName} ${obj.lastName}`;
      let pos=name.toLowerCase().indexOf(value);
      if(pos!==-1) return true;
      return false; 
    });*/    
  }

  

}
