import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFicheTravauxSuiviQuotidienComponent } from './form-fiche-travaux-suivi-quotidien.component';

describe('FormFicheTravauxSuiviQuotidienComponent', () => {
  let component: FormFicheTravauxSuiviQuotidienComponent;
  let fixture: ComponentFixture<FormFicheTravauxSuiviQuotidienComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFicheTravauxSuiviQuotidienComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFicheTravauxSuiviQuotidienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
