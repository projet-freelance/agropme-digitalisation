import { Component, ElementRef, OnInit } from '@angular/core';

declare var MStepper: any;
declare var DynamicTable:any;

@Component({
  selector: 'app-form-fiche-travaux-suivi-quotidien',
  templateUrl: './form-fiche-travaux-suivi-quotidien.component.html',
  styleUrls: ['./form-fiche-travaux-suivi-quotidien.component.css']
})
export class FormFicheTravauxSuiviQuotidienComponent implements OnInit {

  titlePage:string = "Travaux suivi quotidien";
  path:any = [
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Formulaire'
    },
    {
      'link':'#',
      'text':'Fiche'
    },
    {
      'link':'#',
      'text':'Travaux suivi quotidien'
    },
  ];

  constructor(private dashboard:ElementRef) { }

  ngOnInit(): void {
      //stepper
      let steppers = this.dashboard.nativeElement.querySelectorAll('.stepper');
      steppers.forEach(stepper => new MStepper(stepper, {
      // options
      firstActive: 0 // this is the default
      })
    );
    //les tables dynamiques
    let formationtableInstance = new DynamicTable(this.dashboard.nativeElement.querySelector('.table_formation'));
    let languetableInstance = new DynamicTable(this.dashboard.nativeElement.querySelector('.table_langue'),{
            edition:{
                    html:'select',
                    defaultoption:'niveau',
                    options:["Oui","Non"]
            }
    });

  }

}
