import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import { Client } from '../../metiers/entites/client';

import { MoteurProcessusService } from './../../services/processus/moteurprocessus.service';
import { ProcessusType } from '../../models/processustype';
import { Router } from '@angular/router';
declare var $:any;

@Component({
  selector: 'app-new-projet',
  templateUrl: './new-projet.component.html',
  styleUrls: ['./new-projet.component.css']
})
export class NewProjetComponent implements OnInit,AfterViewInit {

  titlePage:String="Nouveau projet";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Projet'
    },
    {
      'link':'#',
      'text':'Nouveau',
    },
  ]

  form:FormGroup;
  
  submittedForm:boolean=false;
  waitingForm:boolean=false;
  errorModalFormMessage:String="";
  projet_type=[
    ProcessusType.SELECTION_PAR_ENTENTE_DIRECT,
    ProcessusType.APPEL_MANIFESTATION_INTERET,
    ProcessusType.SELECTION_PAR_QUALIFICATION
  ];
  datapicker_datenaiss:any= null

  messageFlashMessage:String="";
  showFlashMessage:Boolean=false;
  typeFlashMessage:String="";
  durationFlashMessage=2000;
  
  constructor(
    private dashboard:ElementRef,
    private router:Router,
    private moteurPocess:MoteurProcessusService) { }
  ngAfterViewInit(): void {
    $('select').material_select();
    $('select').on('change',()=>this.form.controls.projet_type.setValue($('select').val().split(' ')[1]))
    $('.datepicker_debut_new_projet').pickadate({
      format:'dd/mm/yyyy',
      onSet:(data) =>{
        let date=new Date(data.select);
        let sdate=`${date.getDate()}/${(date).getMonth()+1}/${date.getFullYear()}`;
        this.form.controls.dateDebut.setValue(sdate)
      }
    });
    $('.datepicker_fin_new_projet').pickadate({
      format:'dd/mm/yyyy',
      onSet:(data) =>{
        let date=new Date(data.select);
        let sdate=`${date.getDate()}/${(date).getMonth()+1}/${date.getFullYear()}`;
        this.form.controls.dateFin.setValue(sdate)
      }
    });
    
  }

  ngOnInit(): void {
    //this.datapicker_datenaiss = M.DatePicker.init(this.dashboard.nativeElement.querySelector('.datepicker_new_projet'),
    //{});
    this.form=new FormGroup({
      title:new FormControl('',[Validators.required]),
      clientMail:new FormControl('',[Validators.required]),
      telClient:new FormControl('',[Validators.required]),
      localisationClient:new FormControl('',[Validators.required]),
      description:new FormControl('',[Validators.required]),
      clientName: new FormControl('',[Validators.required]),
      dateDebut: new FormControl('',[Validators.required]),
      dateFin: new FormControl('',[Validators.required]),
      projet_type: new FormControl(this.projet_type[0],[Validators.required]),
    });
    
  }

  submit()
  {
    if(!this.form.valid)
    {
      this.errorModalFormMessage="Formulaire de creation de projet incomplete. Veuillez le completer et soumettre a nouveau";
      return;
    }
    console.log(this.form.value)
    this.waitingForm=true;
    this.moteurPocess.newProcess(
        this.form.value.projet_type,
        this.form.value.title,
        this.form.value.description,
        Client.fromObject({
            nomPrenom:this.form.value.clientName,
            mail:this.form.value.clientMail,
            tel:this.form.value.telClient,
            localisation:this.form.value.localisationClient,
        }),
        this.form.value.dateDebut,
        this.form.value.dateFin)
        .then((result:ResultStatut)=>
        {
          this.waitingForm=false;
          this.messageFlashMessage="Creation de projet réussie";
          this.typeFlashMessage="success";
          this.showFlashMessage=true;
          setTimeout(() => this.router.navigate(['/dashboard/projet/handler',result.result.id]),2500);
        })
        .catch((error:ResultStatut)=>
        {
          this.waitingForm=false;
          this.messageFlashMessage="Echec de la creation de projet: "+error.message;
          this.typeFlashMessage="danger";
          console.log(error.message)
          this.showFlashMessage=true;
        })
  }
  changeFlashMessageStatut(data)
  {
    this.showFlashMessage=data.show;
  }
  
}
