import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-webmail',
  templateUrl: './webmail.component.html',
  styleUrls: ['./webmail.component.css']
})
export class WebmailComponent implements OnInit {
  titlePage:String="Test de l'ui webmail";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Test'
    },
    {
      'link':'#',
      'text':'Ui',
    },
    {
      'link':'#',
      'text':'Webmail',
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
