import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import { CV } from '../../models/cv';
import { Evaluation } from '../../models/evaluation';
import  { PersonnelDataService } from './../../services/forms/personneldata.service';

declare var $:any;
@Component({
  selector: 'app-list-personnel',
  templateUrl: './list-personnel.component.html',
  styleUrls: ['./list-personnel.component.css']
})
export class ListPersonnelComponent implements OnInit, AfterViewInit{
  titlePage:String="Liste du personnel";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Personnels'
    }
  ];
  formModal:FormGroup;
  submittedModalForm:boolean=false;
  waitingModalForm:boolean=false;
  etat_civil_modal_form=[
    {name:"Célibataire"},
    {name:"Marié"}
  ];

  cvPersonnel:CV[]=[];
  currCVPersonnel:CV[]=[];
  input:FormControl=new FormControl();

  messageFlashMessage:String="";
  showFlashMessage:Boolean=false;
  typeFlashMessage:String="";
  durationFlashMessage=5000;

  errorModalFormMessage:String="";

  modal_box=null;
  constructor(private dashboard:ElementRef ,
    private personnalDataService:PersonnelDataService,
    private router:Router) { }
  ngAfterViewInit(): void {
    $('.modal').modal({onCloseEnd:()=> this.closeModal()});
  }

  ngOnInit(): void {
    this.formModal=new FormGroup({
      fname:new FormControl('',[Validators.required]),
      lname:new FormControl('',[Validators.required])
    });

    this.personnalDataService.subscribeToCV().subscribe((userData)=> {
      this.cvPersonnel=userData;
      this.currCVPersonnel=[...this.cvPersonnel];
    });
    this.personnalDataService.emit("cv");
    
  }

  closeModal()
  {
    this.formModal.reset();
    this.waitingModalForm=false;
    this.errorModalFormMessage="";
    $('.modal').modal('close');
    //this.modal_box.close();
  }

  submitModalForm()
  {
    if( !this.formModal.valid )
    {
      this.waitingModalForm=false;
      this.errorModalFormMessage="Formulaire invalide";
      return;
    }

    let cv:CV=new CV();
    cv.firstName=this.formModal.value.fname;
    cv.lastName=this.formModal.value.lname;

    let evaluation:Evaluation=new Evaluation();
    evaluation.idPersonnel=cv.idPersonnel;
    this.waitingModalForm=true;
    this.personnalDataService.addCV(cv)
    .then((result:ResultStatut)=> this.personnalDataService.addEvaluation(evaluation))
    .then((result:ResultStatut) =>
    {
        this.waitingModalForm=false;
        this.messageFlashMessage="Personnel ajouté avec success";
        this.showFlashMessage=true;
        this.typeFlashMessage="success";
        this.closeModal();
        //redirigé l'utilisateur vers la même page
        this.router.navigate(['dashboard/personnels']);
    })
    .catch((error:ResultStatut)=>
    {
        this.waitingModalForm=false;
        
        this.showFlashMessage=true;
        this.typeFlashMessage="danger";
        this.messageFlashMessage=error.message;
        this.errorModalFormMessage=error.message;
        console.log("Error ",error)
    });
  }
  changeFlashMessageStatut(data)
  {
    this.showFlashMessage=data.show;
  }
  submit(event):void
  {
    let value=this.input.value;
    if(value.length<1)
    {
      this.currCVPersonnel=[...this.cvPersonnel];
      return;
    }
    this.currCVPersonnel=this.cvPersonnel.filter(obj=>{
      let name=`${obj.firstName} ${obj.lastName}`;
      let pos=name.toLowerCase().indexOf(value);
      if(pos!==-1) return true;
      return false; 
    });    
  }
}
