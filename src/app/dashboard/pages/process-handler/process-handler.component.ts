import { AfterViewInit, Component, ComponentFactoryResolver, ElementRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import { Activite } from '../../models/activite';
import { Processus } from '../../models/processus';
import { MoteurProcessusService } from '../../services/processus/moteurprocessus.service';
import { NotificationService } from '../../services/notifications/notification.service';
import { Notification } from './../../models/notification';

declare var $:any;

@Component({
  selector: 'app-process-handler',
  templateUrl: './process-handler.component.html',
  styleUrls: ['./process-handler.component.css'],
})
export class ProcessHandlerComponent implements OnInit, AfterViewInit {
  titlePage:String="Processus: ";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Projet'
    },
    {
      'link':'#',
      'text':'Traitement',
    },
  ]

  refCurrentProccess:String="";
  currentProcess:Processus=null;
  findProcessus:boolean=true;

  messageFlashMessage:String="";
  showFlashMessage:Boolean=false;
  typeFlashMessage:String="";
  durationFlashMessage=2000;

  isExecuteur:boolean=true;

  isEndProcess:boolean=false;

  refCurrentForm=null;

  dataForm=null;

  loading:boolean=true;

  subscriptionProcess=null

  //@ViewChild('container', {read: ViewContainerRef}) dashboardDirective;
  @ViewChild('dashboard_host',{read: ViewContainerRef}) dashboard_host:ViewContainerRef;
  constructor(
    private dashboard:ElementRef,
    private actRoute:ActivatedRoute,
    private moteurPocess:MoteurProcessusService,
    private authService:AuthService,
    private notificationService:NotificationService,
    private router:Router,
    private componentFactoryResolver: ComponentFactoryResolver) {}


  ngOnInit(): void {
    
    this.actRoute.paramMap.subscribe(params => { 
      this.refCurrentProccess = params.get('process_id'); 
    });

    
  }
  ngAfterViewInit() 
  {
    this.dashboard_host.clear();
    $('.modal').modal();
    $('.tabs').tabs({swipeable:true});

    this.subscriptionProcess=this.moteurPocess.subscribeToProcess().subscribe(() =>
    {
      this.loading=false;
      console.log("subscribe kher")
      if(!this.moteurPocess.existProcess(this.refCurrentProccess))
      {
        this.findProcessus=false;
      }
      else
      {
        this.currentProcess = this.moteurPocess.getProcess(this.refCurrentProccess);
        /*if(this.currentProcess.getCurrentActivity().idExecuteur!=this.authService.getUser().id)
        {
          this.isExecuteur=false;
          return;
        }*/
        if(this.currentProcess.isEnd())
        {
          this.isEndProcess = true;
        }
        else
        {
          this.titlePage=`Processus: ${this.currentProcess.title.toString()}`;
          //console.log(this.currentProcess.type)
          this.loadComponent(); 
        }
            
      }
       
    });
    
    
  }
  async loadComponent()
  {
    let newComponent = await import(`./../${this.currentProcess.getCurrentActivity().formulaire.toString()}`);
    let keyClass="";
    for (let key in newComponent) keyClass=key;

    let factory = this.componentFactoryResolver.resolveComponentFactory(newComponent[keyClass]);
    this.dashboard_host.clear();
    this.refCurrentForm = this.dashboard_host.createComponent(factory);
    this.refCurrentForm.instance.processId=this.currentProcess.id;
    this.refCurrentForm.instance.eventSubmit.subscribe((data)=>this.validProcess(data));

  }

  changeFlashMessageStatut(data):void
  {
    this.showFlashMessage=data.show;
  }
  validProcess(data)
  {
    this.moteurPocess.nextProcessStep(this.currentProcess.id,data)
    .then((result:ResultStatut)=>{
      
      let activite:Activite=result.result;
      this.messageFlashMessage="Tache effectué avec success";
          this.showFlashMessage=true;
          this.typeFlashMessage="success";
          this.refCurrentForm.instance.waitingForm=false;
          this.dashboard_host.clear();
          this.router.navigate(['/dashboard/projet/handler',this.currentProcess.id]);
          
      /*if(activite.idExecuteur!=this.authService.getUser().id)
      {
        let notif:Notification = new Notification("Systeme",activite.idExecuteur.toString(),`Activité: ${activite.title}`);
        notif.idProcess = this.currentProcess.id;
        this.notificationService.sendNewNotification(
          notif,activite.idExecuteur)
        .then((result:ResultStatut)=>{
          this.messageFlashMessage="Tache effectué avec success";
          this.showFlashMessage=true;
          this.typeFlashMessage="success";
          this.refCurrentForm.instance.waitingForm=false;
          this.dashboard_host.clear();
          this.ngOnDestroy();
          this.ngOnInit();
        })
        .catch((error:ResultStatut)=>{
          this.messageFlashMessage="La notification n'a pas pus être envoyé a l'exécuteur de la tâche suivante";
          this.showFlashMessage=true;
          this.typeFlashMessage="warning";
          this.refCurrentForm.instance.waitingForm=false;
          this.dashboard_host.clear();
          this.ngOnDestroy();
          this.ngOnInit();
        });
      }
      else
      {
        this.messageFlashMessage="Tache effectué avec success";
        this.showFlashMessage=true;
        this.typeFlashMessage="success";
        this.refCurrentForm.instance.waitingForm=false;
        this.dashboard_host.clear();
        this.ngOnDestroy();
        this.ngOnInit();
      }*/
      
    })
    .catch((error:ResultStatut)=>{
      this.messageFlashMessage=`Erreur: ${error.message}`;
      this.showFlashMessage=true;
      this.typeFlashMessage="danger";
      this.refCurrentForm.instance.waitingForm=false;
    });
    
  }
  ngOnDestroy() {
    if(this.refCurrentForm) this.refCurrentForm.destroy(); 
    this.subscriptionProcess.unsubscribe();
  }

  showModalDetails()
  {
    $('.modal').modal('open');
  }

}
