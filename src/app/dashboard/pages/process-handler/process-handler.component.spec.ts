import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessHandlerComponent } from './process-handler.component';

describe('ProcessHandlerComponent', () => {
  let component: ProcessHandlerComponent;
  let fixture: ComponentFixture<ProcessHandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessHandlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessHandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
