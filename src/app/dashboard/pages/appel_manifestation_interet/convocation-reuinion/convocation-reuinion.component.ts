import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-convocation-reuinion',
  templateUrl: './convocation-reuinion.component.html',
  styleUrls: ['./convocation-reuinion.component.css']
})
export class ConvocationReuinionComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  @Input() processId="";

  listUser=[];

  formInput=
  [
    [
      {
        id:"objetReuinion",
        html:"input",
        type:"text",
        text:'Objet de la réuinion',
        container_class:"col s6",
        icon:"border_color"
        //validators:[Validators.required]       
      },   
    ],
    [
      {
        id:"resume",
        html:"input",
        type:"text",
        text:'resumé de la réuinion',
        container_class:"col s12", 
        icon:"border_color"
        //validators:[Validators.required]       
      },
    ]
  ]
  constructor( ) { }

  ngOnInit(): void {
  }
  submitForm(data)
  {
    this.waitingForm=true;
    this.eventSubmit.emit({
      objet:data.objetReuinion,
      resume:data.resume
    });
  }
}
