import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvocationReuinionComponent } from './convocation-reuinion.component';

describe('ConvocationReuinionComponent', () => {
  let component: ConvocationReuinionComponent;
  let fixture: ComponentFixture<ConvocationReuinionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvocationReuinionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvocationReuinionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
