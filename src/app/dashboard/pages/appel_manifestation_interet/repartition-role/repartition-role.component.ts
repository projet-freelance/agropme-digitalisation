import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-repartition-role',
  templateUrl: './repartition-role.component.html',
  styleUrls: ['./repartition-role.component.css']
})
export class RepartitionRoleComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  @Input() processId="";

  formInput=
  [
    [
      {
        id:"documentRepartition",
        html:"input",
        type:"text",
        text:'Docuement de repartition de role: Lien Google Drive',
        container_class:"col s12", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
    ],
  ]
  constructor() { }

  ngOnInit(): void {
  }
  submitForm(data)
  {
    this.waitingForm=true;
    this.eventSubmit.emit({
      docRole:data.documentRepartition
    });
  }
}
