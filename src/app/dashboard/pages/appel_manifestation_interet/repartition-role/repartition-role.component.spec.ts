import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepartitionRoleComponent } from './repartition-role.component';

describe('RepartitionRoleComponent', () => {
  let component: RepartitionRoleComponent;
  let fixture: ComponentFixture<RepartitionRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepartitionRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepartitionRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
