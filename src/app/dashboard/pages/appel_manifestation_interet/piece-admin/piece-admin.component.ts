import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-piece-admin',
  templateUrl: './piece-admin.component.html',
  styleUrls: ['./piece-admin.component.css']
})
export class PieceAdminComponent implements OnInit {
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  @Input() processId="";

  formInput=
  [
    [
      {
        id:"docJuridique",
        html:"input",
        type:"text",
        text:'Document juridique: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
      {
        id:"docFiscaux",
        html:"input",
        type:"text",
        text:'Document fiscaux: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },      
    ],
    [
      {
        id:"docJustificatif",
        html:"input",
        type:"text",
        text:'Document justificatif de la capacité financiere du cabinet: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
      {
        id:"docContrat",
        html:"input",
        type:"text",
        text:'Contrat de service: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
    ],
    [
      {
        id:"docAttestationService",
        html:"input",
        type:"text",
        text:'Attestation de service fait: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
      {
        id:"docAvisManifestation",
        html:"input",
        type:"text",
        text:'Reponse a l\'avis de manifestation d\'intérêt: Lien Google Drive',
        container_class:"col s6", 
        icon:"cloud" 
        //validators:[Validators.required]       
      },
    ]
  ]
  constructor() { }

  ngOnInit(): void {
  }
  submitForm(data)
  {
    this.waitingForm=true;
    this.eventSubmit.emit({
      doc:{
        juridique:data.docJuridique,
        fiscaux:data.docFiscaux,
        justificatif:data.docJustificatif,
        contrat:data.docContrat,
        attestation:data.docAttestationService
      }
    });
  }
}
