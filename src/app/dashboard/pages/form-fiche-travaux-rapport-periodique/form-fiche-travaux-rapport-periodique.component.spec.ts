import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFicheTravauxRapportPeriodiqueComponent } from './form-fiche-travaux-rapport-periodique.component';

describe('FormFicheTravauxRapportPeriodiqueComponent', () => {
  let component: FormFicheTravauxRapportPeriodiqueComponent;
  let fixture: ComponentFixture<FormFicheTravauxRapportPeriodiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFicheTravauxRapportPeriodiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFicheTravauxRapportPeriodiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
