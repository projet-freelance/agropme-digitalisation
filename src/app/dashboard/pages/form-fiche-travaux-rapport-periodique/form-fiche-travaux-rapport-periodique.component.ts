import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-fiche-travaux-rapport-periodique',
  templateUrl: './form-fiche-travaux-rapport-periodique.component.html',
  styleUrls: ['./form-fiche-travaux-rapport-periodique.component.css']
})
export class FormFicheTravauxRapportPeriodiqueComponent implements OnInit {

  titlePage:String="Fiche: travaux rapport periodique";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Formulaire'
    },
    {
      'link':'#',
      'text':'Fiche',
    },
    {
      'link':'#',
      'text':'Fiche travaux rapport periodique',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
