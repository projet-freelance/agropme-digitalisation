import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailboxReadmailComponent } from './mailbox-readmail.component';

describe('MailboxReadmailComponent', () => {
  let component: MailboxReadmailComponent;
  let fixture: ComponentFixture<MailboxReadmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailboxReadmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailboxReadmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
