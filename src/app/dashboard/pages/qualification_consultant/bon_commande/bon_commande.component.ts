import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bon-commande',
  templateUrl: './bon_commande.component.html',
  styleUrls: ['./bon_commande.component.css'],
})
export class BonCommandeComponent implements OnInit{
  @Output() eventSubmit=new EventEmitter<Object>();
  @Input() data="";
  @Input() waitingForm=false;
  formInput=[
    [
      {
        id:"numCommande",
        html:"input",
        type:"text",
        text:'Numero de la commande',
        container_class:"col s6", 
        validators:[Validators.required,Validators.minLength(5)]       
      },
      {
        id:"libelleCommande",
        html:"input",
        type:"text",
        text:'Libellé de la commande',
        container_class:"col s6", 
        validators:[Validators.required,Validators.minLength(4)]       
      },      
    ],
    [
      {
        id:"avisImposition",
        html:"input",
        type:"checkbox",
        text:'Editer l\'avis d\'imposition',
        container_class:"col s6",       
        validators:[Validators.required] 
      },
      {
        id:"editEtTransmetOrdreVirement",
        html:"input",
        type:"checkbox",
        text:'Edition de l\'ordre de virement',
        container_class:"col s6",       
        validators:[Validators.required] 
      }
    ]
  ]

  constructor() {
   }

  ngOnInit(): void {

  }

  submitForm(data)
  {
    this.eventSubmit.emit({
      commande: {
        numero:data.numCommande,
        libelle:data.libelleCommande
      },
      avisImposition:data.avisImposition,
      ordreVirement:data.editEtTransmetOrdreVirement
    });
  }
}
