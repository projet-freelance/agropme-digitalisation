import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-fiche-rapport-mensuel-vente',
  templateUrl: './form-fiche-rapport-mensuel-vente.component.html',
  styleUrls: ['./form-fiche-rapport-mensuel-vente.component.css']
})
export class FormFicheRapportMensuelVenteComponent implements OnInit {

  titlePage:string = "Fiche: Rapport mensuel ventes";
  path:any = [
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Formulaire'
    },
    {
      'link':'#',
      'text':'Fiche'
    },
    {
      'link':'#',
      'text':'rapport mensuel vente'
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
