import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFicheRapportMensuelVenteComponent } from './form-fiche-rapport-mensuel-vente.component';

describe('FormFicheRapportMensuelVenteComponent', () => {
  let component: FormFicheRapportMensuelVenteComponent;
  let fixture: ComponentFixture<FormFicheRapportMensuelVenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFicheRapportMensuelVenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFicheRapportMensuelVenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
