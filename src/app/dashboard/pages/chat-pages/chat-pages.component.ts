import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat-pages',
  templateUrl: './chat-pages.component.html',
  styleUrls: ['./chat-pages.component.css']
})
export class ChatPagesComponent implements OnInit {

  titlePage:String="Partage de ressource";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Discussions'
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
