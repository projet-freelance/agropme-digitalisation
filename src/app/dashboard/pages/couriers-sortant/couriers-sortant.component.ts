import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import {CourierSortant } from './../../models/courier';
import { CourierService } from './../../services/courier/courier.service';


declare var $:any;
@Component({
  selector: 'app-couriers-sortant',
  templateUrl: './couriers-sortant.component.html',
  styleUrls: ['./couriers-sortant.component.css']
})
export class CouriersSortantComponent implements OnInit, AfterViewInit {
  titlePage:String="Liste des couriers sortant";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Couriers'
    },
    {
      'link':'#',
      'text':'Sortant'
    }
  ];

  courierFindInput:FormControl;

  listeSortant:CourierSortant[] = [];
  currListeSortant:CourierSortant[] = [];

  formModalSortie:FormGroup;
  submittedModalForm:boolean=false;
  waitingModalForm:boolean=false;
  isConfidentielle=[
    {name:"Confidentiel"},
    {name:"Non confidentiel"}
  ];

  input:FormControl=new FormControl();

  messageFlashMessage:String="";
  showFlashMessage:Boolean=false;
  typeFlashMessage:String="";
  durationFlashMessage=5000;

  errorModalFormMessage:String="";

  modal_box=null;
  constructor( private router:Router,
    private courierSortant: CourierService) { 

  }

  ngOnInit(): void {
    this.formModalSortie=new FormGroup({
      destinataire:new FormControl('',[Validators.required]),
      dateDepart:new FormControl('',[Validators.required]),
      objet:new FormControl('',[Validators.required]),
      type:new FormControl('',[Validators.required]),
      observation:new FormControl('',[Validators.nullValidator]),
      option:new FormControl('',[Validators.required])
    });

    this.courierSortant.subscribeCourierSortant().subscribe((courierData)=> {
      this.listeSortant=courierData;
      this.currListeSortant=[...this.listeSortant];
    });
    this.courierSortant.emit("courierSortant");
    
  }

  ngAfterViewInit(): void {
    $('.modal').modal({onCloseEnd:()=> this.closeModal()});
    $('#type').material_select();
    $('#type').on('change',()=> this.formModalSortie.controls.type.setValue($('#type').val()));
    
    $('#option').material_select();
    $('#option').on('change',()=> this.formModalSortie.controls.option.setValue($('#option').val()));

    $('.datepicker_new_courier').pickadate({
      format:'dd/mm/yyyy',
      onSet:(data)=>{
        console.log(data)
        let date = new Date(data.select);
        let sdate=`${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
        this.formModalSortie.controls.dateDepart.setValue(sdate);
      }
    });
  }

  closeModal()
  {
    this.formModalSortie.reset();
    this.waitingModalForm=false;
    this.errorModalFormMessage="";
    $('.modal').modal('close');
    //this.modal_box.close();
  }

  submitModalForm()
  {
    if( !this.formModalSortie.valid )
    {
      this.waitingModalForm=false;
      this.errorModalFormMessage="Formulaire invalide";
      return;
    }

    let courierSortant: CourierSortant = new CourierSortant();
    courierSortant.destinataire=this.formModalSortie.value.destinataire;
    courierSortant.dateSorti=this.formModalSortie.value.dateDepart;
    courierSortant.object=this.formModalSortie.value.objet;
    courierSortant.observation=this.formModalSortie.value.observation;
    courierSortant.voiTranmission=this.formModalSortie.value.type;
    courierSortant.isConfidentielle=this.formModalSortie.value.option;
    console.log(this.formModalSortie.value);
    this.waitingModalForm=true;

    this.courierSortant.saveCourierSortant(courierSortant)
    .then((result:ResultStatut)=>
    {
      console.log("saved");
        this.messageFlashMessage="Courier sortant ajouté avec success";
        this.showFlashMessage=true;
        this.typeFlashMessage="success";
        this.closeModal();
        this.router.navigate(['/dasboard/couriers/sortant']);
    })
    .catch((error:ResultStatut)=>
    {        
        console.log("no saved");
        this.showFlashMessage=true;
        this.typeFlashMessage="danger";
        this.waitingModalForm=false;
        this.messageFlashMessage=error.message;
        this.errorModalFormMessage=error.message;
    });
  }
  findCourier(data)
  {

  }
  changeFlashMessageStatut(data)
  {
    this.showFlashMessage=data.show;
  }
  submit(event):void
  {
    // let value=this.input.value;
    // if(value.length<1)
    // {
    //   this.currCVPersonnel=[...this.cvPersonnel];
    //   return;
    // }
    // this.currCVPersonnel=this.cvPersonnel.filter(obj=>{
    //   let name=`${obj.firstName} ${obj.lastName}`;
    //   let pos=name.toLowerCase().indexOf(value);
    //   if(pos!==-1) return true;
    //   return false; 
    // });    
  }
}
