import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CouriersSortantComponent } from './couriers-sortant.component';

describe('CouriersSortantComponent', () => {
  let component: CouriersSortantComponent;
  let fixture: ComponentFixture<CouriersSortantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CouriersSortantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CouriersSortantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
