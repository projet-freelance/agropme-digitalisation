import { getDateNow } from 'src/app/utils/date';
import { EntityID } from './../../models/EntityID';

export class NotificationStatut
{
    static read:boolean=true;
    static unread:boolean=false;
}

export class NotificationType
{
    static TASK="task";
}

export class Notification
{
    id:String=(new EntityID()).toString();
    sender:string;
    receiver:string;
    content:any="";
    disponibilite:any[];
    is_read:boolean;
    date=getDateNow();
    idProcess:String="";
    type:String=NotificationType.TASK;
    constructor(sender:string='',receiver:string='',content="",read:boolean=NotificationStatut.unread)
    {
        this.sender=sender;
        this.receiver=receiver;
        this.content=content;
        this.is_read=read;
    }
    setRead(read:boolean=NotificationStatut.read)
    {
        this.is_read=read;
    }
    static fromObject(obj):Notification
    {
        let sender=obj.sender?obj.sender:'';
        let receiver=obj.receiver?obj.receiver:'';
        let content=obj.content?obj.content:"";
        let is_read=obj.is_read?obj.is_read:NotificationStatut.unread;
        let id=obj.id?obj.id:(new EntityID()).toString()
        let notif =new Notification(sender,receiver,content,is_read);
        notif.id=id;
        notif.type=obj.type?obj.type:NotificationType.TASK;
        notif.date = obj.date;
        notif.idProcess = obj.idProcess ? obj.idProcess : "";
        return notif;
    }
    toString()
    {
        return {
            sender:this.sender,
            receiver:this.receiver,
            content:this.content,
            is_read:this.is_read,
            id:this.id,
            type:this.type,
            date:this.date,
            idProcess:this.idProcess,
        };
    }
}