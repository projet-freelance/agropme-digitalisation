import { Activite } from "./activite";
import { Client } from "./../metiers/entites/client";
import { EntityID } from 'src/app/models/EntityID';


export abstract class Processus
{
    type:any={};
    title:String="";
    description:String="";
    workflow:Activite[]=[];
    step=0;
    client:Client;
    dateDebut="";
    dateFin=""
    id:String=(new EntityID()).toString();
    
    getCurrentActivity():Activite
    {
        return this.workflow[this.step];
    }
    nextActivity():Activite
    {
        if(this.isEnd()) return null;
        this.step++;
        return this.getCurrentActivity();        
    }
    getDataActivity(index=-1):any
    {
        if(index==-1) return this.getCurrentActivity().data;
        if(index-1<0  || index-1>this.workflow.length) return null;
        return this.workflow[index-1].data;
    }
    isEnd():boolean
    {
        return this.step+1==this.workflow.length;
    }
    toString()
    {
        let workflowString=this.workflow.map((activite:Activite)=>activite.toString());
        return {
            id:this.id,
            type:this.type,
            dateDebut:this.dateDebut,
            dateFin:this.dateFin,
            title:this.title,
            description:this.description,
            workflow:workflowString,
            step:this.step,
            client:this.client.toString(),
        }
    }
    fromObject(obj:any)
    {
        if(obj.workflow==undefined) obj.workflow=[];
        this.workflow= obj.workflow.map(activite=>Activite.fromObject(activite));
        this.step=obj.step;
        this.id=obj.id;
    }
    abstract constructWorkflow():void;
}

