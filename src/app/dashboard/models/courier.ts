import { EntityID } from './../../models/EntityID';

export class CourieModeTransport
{
    static ELECTRONIQUE="electronique";
    static POSTAL="postal";
}

export class CourierEntrant
{
    id:String=(new EntityID()).toString();
    // idPersonnel:String=(new EntityID()).toString();
    dateArrive:Date=new Date();
    object:String="";
    observation:String="";
    destinataire:String="";
    source:String="";
    voiTranmission:String="";
    isConfidentielle:boolean=false;
    serviceDestinataire:String="";

    toString():any
    {
        return {
            id:this.id,
            // idPersonnel:this.idPersonnel,
            dateArrive:this.dateArrive,
            voiTranmission:this.voiTranmission,
            object:this.object,
            source:this.source,
            observation:this.observation,
            destinataire:this.destinataire,
            isConfidentielle:this.isConfidentielle,
            serviceDestinataire:this.serviceDestinataire,
        };
    }
    static fromObject(obj:any):CourierEntrant
    {
        let ce=new CourierEntrant();
        ce.id=obj['id'] || (new EntityID()).toString();
        ce.source=obj['source'] || "";
        ce.serviceDestinataire=obj['serviceDestinataire'] || "";
        // ce.idPersonnel=obj['idPersonnel'] || (new EntityID()).toString();
        ce.dateArrive=obj['dateArrive'] || "";
        ce.object=obj['object'] || "";
        ce.observation=obj['observation'] || "";
        ce.destinataire=obj['destinataire'] || "";
        ce.isConfidentielle=obj['isConfidentielle'] || "";
        ce.voiTranmission=obj['voiTranmission'] || CourieModeTransport.ELECTRONIQUE;
        return ce;
    }
    isSortant()
    {
        return false;
    }
}

export class CourierSortant
{
    id:String=(new EntityID()).toString();
    dateSorti:Date=new Date();
    object:String="";
    observation:String="";
    voiTranmission:String="";
    destinataire:String="";
    isConfidentielle:boolean=false;
    toString():any
    {
        return {
            id:this.id,
            dateSorti:this.dateSorti,
            object:this.object,
            observation:this.observation,
            destinataire:this.destinataire,
            voiTranmission:this.voiTranmission,
            isConfidentielle:this.isConfidentielle
        };
    }
    static fromObject(obj:any):CourierSortant
    {
        let cs=new CourierSortant();
        cs.id=obj['id'];
        cs.dateSorti=obj['dateSorti'];
        cs.object=obj['object'];
        cs.observation=obj['observation'];
        cs.voiTranmission=obj['voiTranmission'];
        cs.destinataire=obj['destinataire'];
        cs.isConfidentielle=obj['isConfidentielle'];
        return cs;
    }
    isSortant()
    {
        return true;
    }
}

