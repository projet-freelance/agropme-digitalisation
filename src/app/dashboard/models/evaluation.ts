import { getDateNow } from 'src/app/utils/date';
import { EntityID } from "../../models/EntityID";

export class Evaluation
{
    dateMiseAJour:String=getDateNow().date;
    devaluateur:String="";
    id:String=(new EntityID()).toString();
    idPersonnel:String=(new EntityID()).toString();

    _formationInitial=0;
    _formationAcademique=0;
    _reputationInstitution=0;

    _competenceExperience=0;
    _traveauRealise=0;
    _reputationClient=0;
    _experiencePratique=0;
    _experienceFormateur=0;
    _experienceConsultant=0;

    _dispoOutilsTravailDeBase=0;
    _vehiculePerso=0;
    _pc=0;
    _maitriseTraitText=0;
    _telOuFax=0;

    _qualitePersonnel=0;
    _reputation=0;
    _comportement=0;

    _dispo=0;

    set formationInitial(initForm)
    {
        if(initForm>10 || initForm<0 ) throw new Error("Invalide formationInitial value")
        this._formationInitial=initForm;
    }

    set formationAcademique(initForm)
    {
        if(initForm>10 || initForm<0 ) throw new Error("Invalide formationAcademique value")
        this._formationAcademique=initForm;
    }

    set reputationInstitution(initForm)
    {
        if(initForm>10 || initForm<0 ) throw new Error("Invalide reputationInstitution value")
        this._reputationInstitution=initForm;
    }
    set competenceExperience(initForm)
    {
        if(initForm>10 || initForm<0 ) throw new Error("Invalide competenceExperience value")
        this._competenceExperience=initForm;
    }
    set traveauRealise(initForm)
    {
        if(initForm>50 || initForm<0 ) throw new Error("Invalide traveauRealise value")
        this._traveauRealise=initForm;
    }
    set reputationClient(initForm)
    {
        if(initForm>50 || initForm<0 ) throw new Error("Invalide reputationClient value")
        this._reputationClient=initForm;
    }
    set experiencePratique(initForm)
    {
        if(initForm>50 || initForm<0 ) throw new Error("Invalide experiencePratique value")
        this._experiencePratique=initForm;
    }
    set experienceFormateur(initForm)
    {
        if(initForm>50 || initForm<0 ) throw new Error("Invalide experienceFormateur value")
        this._experienceFormateur=initForm;
    }
    set experienceConsultant(initForm)
    {
        if(initForm>50 || initForm<0 ) throw new Error("Invalide experienceConsultant value")
        this._experienceConsultant=initForm;
    }
    set dispoOutilsTravailDeBase(initForm)
    {
        if(initForm>25 || initForm<0 ) throw new Error("Invalide dispoOutilsTravailDeBase value")
        this._dispoOutilsTravailDeBase=initForm;
    }
    set vehiculePerso(initForm)
    {
        if(initForm>25 || initForm<0 ) throw new Error("Invalide vehiculePerso value")
        this._vehiculePerso=initForm;
    }
    set pc(initForm)
    {
        if(initForm>25 || initForm<0 ) throw new Error("Invalide pc value")
        this._pc=initForm;
    }
    set maitriseTraitText(initForm)
    {
        if(initForm>25 || initForm<0 ) throw new Error("Invalide reputationInstitution value")
        this._maitriseTraitText=initForm;
    }
    set telOuFax(initForm)
    {
        if(initForm>25 || initForm<0 ) throw new Error("Invalide telOuFax value")
        this._telOuFax=initForm;
    }
    set qualitePersonnel(initForm)
    {
        if(initForm>10 || initForm<0 ) throw new Error("Invalide qualitePersonnel value")
        this._qualitePersonnel=initForm;
    }
    set reputation(initForm)
    {
        if(initForm>10 || initForm<0 ) throw new Error("Invalide reputation value")
        this._reputation=initForm;
    }
    set comportement(initForm)
    {
        if(initForm>10 || initForm<0 ) throw new Error("Invalide comportement value")
        this._comportement=initForm;
    }
    set dispo(initForm)
    {
        if(initForm>10 || initForm<0 ) throw new Error("Invalide dispo value")
        this._dispo=initForm;
    }

    toString()
    {
        return {
            dateMiseAJour:this.dateMiseAJour,
            devaluateur:this.devaluateur,
            id:this.id,
            idPersonnel:this.idPersonnel,
            formation:{
                academique:this._formationAcademique,
                reputation:this._reputationInstitution
            },
            competence:{        
                traveauRealise:this._traveauRealise,
                reputationClient:this._reputationClient,
                experiencePratique:this._experiencePratique,
                experienceFormateur:this._experienceFormateur,
                experienceConsultant:this._experienceConsultant,
            },
            disponibilite:{
                vehiculePerso:this._vehiculePerso,
                pc:this._pc,
                maitriseTraitText:this._maitriseTraitText,
                telOuFax:this._telOuFax
            },
            qualite:{
                reputation:this._reputation,
                comportement:this._comportement,
            },
            dispo:this._dispo
        }
    }

    static fromObject(obj:any):Evaluation
    {
        let ev:Evaluation=new Evaluation();

        ev.dateMiseAJour=obj.dateMiseAJour;
        ev.devaluateur=obj.devaluateur;
        ev.id=obj.id;
        ev.idPersonnel=obj.idPersonnel;

        //ev.formationInitial=obj.formation.initial;
        ev.formationAcademique=obj.formation.academique;
        ev.reputationInstitution=obj.formation.reputation;

        ev.competenceExperience=obj.competence.experience;
        ev.traveauRealise=obj.competence.traveauRealise;
        ev.reputationClient=obj.competence.reputationClient;
        ev.experiencePratique=obj.competence.reputationPratique;
        ev.experienceFormateur=obj.competence.experienceFormateur;
        ev.experienceConsultant=obj.competence.experienceConsultant;

        //ev.dispoOutilsTravailDeBase=obj.disponibilite.outilsBase;
        ev.vehiculePerso=obj.disponibilite.vehiculePerso;
        ev.pc=obj.disponibilite.pc;
        ev.maitriseTraitText=obj.disponibilite.maitriseTraitText;
        ev.telOuFax=obj.disponibilite.telOuFax;
        
        //ev.qualitePersonnel=obj.qualite.personnel;
        ev.reputation=obj.qualite.reputation;
        ev.comportement=obj.qualite.comportement;
    
        ev.dispo=obj.dispo;
        
        return ev;
    }

    total()
    {
        return this.totalFormation() + 
            this.totalCompetence() + 
            this.totalDisponibilite() + 
            this.totalQualite() +
            this._dispo;
    }
    totalFormation()
    {
        return this._formationAcademique+this._reputationInstitution;
    }
    totalCompetence()
    {
        return this._traveauRealise + 
            this._reputationClient +
            this._experiencePratique +
            this._experienceFormateur + 
            this._experienceConsultant;
    }
    totalDisponibilite()
    {
        return this._vehiculePerso +
            this._pc +
            this._maitriseTraitText +
            this._telOuFax;
    }

    totalQualite()
    {
        return this._qualitePersonnel +
            this._reputation +
            this._comportement;
    }
}
