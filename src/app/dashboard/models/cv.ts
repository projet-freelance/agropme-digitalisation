import { EntityID } from './../../models/EntityID';

export class CV
{
    id:String=(new EntityID()).toString();
    idPersonnel:String=(new EntityID()).toString();
    firstName:String="";
    etatCivil:String="Célibataire";
    lastName:String="";
    dateNaiss:String="";
    nationnalite:String="";
    formationAcademique:any[]=[];
    formationComplementaire:String="";
    communication:any[]=[];
    appartOrgaPro:String="";
    autreCompet:String="";
    fonctionActuelle:any[]=[];
    autreInfosUtils:String="";
    publicationRecherche:any[]=[];

    toString()
    {
       return {
           id:this.id,
           idPersonnel:this.idPersonnel,
           personnel:{
                nom:this.firstName,
                prenom:this.lastName,
                dateNaiss:this.dateNaiss,
                nationnalite:this.nationnalite,
                etat_civil:this.etatCivil
           },
           formation:{
               academique:this.formationAcademique,
               complementaire:this.formationComplementaire,
           },
           communication:this.communication,
           vie_pro:{
               organisme_pro:this.appartOrgaPro,
               autres_conpet:this.autreCompet,
               fonction_actuelle:this.fonctionActuelle
           },
           autres_infos:this.autreCompet,
           pub_recherche:this.publicationRecherche
       } 
    };

    static fromObject(obj:any):CV
    {
        let cv=new CV();
        cv.id=obj["id"] || (new EntityID()).toString();
        cv.idPersonnel=obj["idPersonnel"] || (new EntityID()).toString()
        cv.etatCivil=obj["personnel"]["etat_civil"] || "";
        cv.firstName=obj["personnel"]["nom"] || "";
        cv.lastName=obj["personnel"]["prenom"] || "";
        cv.dateNaiss=obj["personnel"]["dateNaiss"] || "";
        cv.nationnalite=obj["personnel"]["nationnalite"] || "";
        cv.formationAcademique=obj["formation"]["academique"] || [];
        cv.formationComplementaire=obj["formation"]["complementaire"] || "";
        cv.communication=obj["communication"] || [];
        cv.appartOrgaPro=obj["vie_pro"]["organisme_pro"] || "";
        cv.autreCompet=obj["vie_pro"]["autres_conpet"] || "";
        cv.fonctionActuelle=obj["vie_pro"]["fonction_actuelle"] || [];
        cv.autreInfosUtils=obj["autres_infos"] || "";
        cv.publicationRecherche=obj["pub_recherche"] || [];
        return cv;
    }
}