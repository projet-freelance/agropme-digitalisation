import { getDateNow } from 'src/app/utils/date';
import { Activite } from '../models/activite';
import { ProcessusType } from '../models/processustype';
import { Processus } from './../models/processus';
import { CompteExecuteur } from './compte_executeur';
import { Client } from './entites/client';

export class ProcessusSelectionParEntenteDirect extends Processus
{
    constructor(title:String,description:String,client:Client,dateDebut,dateFin)
    {
        super();
        this.client=client;
        this.description=description;
        this.title=title;
        this.dateDebut=dateDebut;
        this.type=ProcessusType.SELECTION_PAR_ENTENTE_DIRECT;
        this.dateFin=dateFin;
        this.constructWorkflow();
    }

    constructWorkflow(): void {
        let act1 = new Activite();
        act1.title="Traitement de la proposition du client";
        act1.description="Traitment de la prososition du client dans le cas ou AGRO-PME est intéressé par le projet";
        act1.executeur=CompteExecuteur.DIRECTEUR_GENERAL.nom;
        act1.idExecuteur=CompteExecuteur.DIRECTEUR_GENERAL.id;
        act1.formulaire="entente_directe/traitement-proposition-client/traitement-proposition-client.component";
        this.workflow.push(act1);

        let act2 = new Activite();
        act2.title="Selection du personnel a affecter au projet";
        act2.description="Selection du personnel a affecter au projet";
        act2.executeur=CompteExecuteur.RSAF.nom;
        act2.idExecuteur=CompteExecuteur.RSAF.id;
        act2.formulaire="entente_directe/gestion-personnel-choisi/gestion-personnel-choisi.component";
        //act1.id="5fe962c25933b99468a6f68b";
        this.workflow.push(act2);

        let act3 = new Activite();
        act3.title="Gestion des documnent lié aux personnels a affecter au projet";
        act3.description="Gestion des documnent lié aux personnels au projet";
        act3.executeur=CompteExecuteur.RSAF.nom;
        act3.idExecuteur=CompteExecuteur.RSAF.id;
        act3.formulaire="entente_directe/doc-personnel-choisi/doc-personnel-choisi.component";
        //act1.id="5fe962c25933b99468a6f68b";
        this.workflow.push(act3);

        let act4 = new Activite();
        act4.title="Elaboration des chronogrammes";
        act4.description="Elaboration du chronogramme de production et de mise oeuvre des activités";
        act4.executeur=CompteExecuteur.SERVICE_TECHNIQUE.nom;
        act4.idExecuteur=CompteExecuteur.SERVICE_TECHNIQUE.id;
        act4.formulaire="entente_directe/chronogramme/chronogramme.component";
        //act1.id="5fe962c25933b99468a6f68b";
        this.workflow.push(act4);

        let act5 = new Activite();
        act5.title="Elaboration des documents administratif";
        act5.description="Elaboration des documents administratif";
        act5.executeur=CompteExecuteur.RSAF.nom;
        act5.idExecuteur=CompteExecuteur.RSAF.id;
        act5.formulaire="entente_directe/doc-admin/doc-admin.component";
        //act1.id="5fe962c25933b99468a6f68b";
        this.workflow.push(act5);

        let act6 = new Activite();
        act6.title="Etablissement de l'offre financiere";
        act6.description="Etablissement de l'offre financiere";
        act6.executeur=CompteExecuteur.RSAF.nom;
        act6.idExecuteur=CompteExecuteur.RSAF.id;
        act6.formulaire="entente_directe/offre-financiere/offre-financiere.component";
        //act1.id="5fe962c25933b99468a6f68b";
        this.workflow.push(act6);

        let act7 = new Activite();
        act7.title="Notification du personnel";
        act7.description="Notification du personnel selectionné";
        act7.executeur=CompteExecuteur.RSAF.nom;
        act7.idExecuteur=CompteExecuteur.RSAF.id;
        act7.formulaire="entente_directe/notifi-personnel/notifi-personnel.component";
        //act1.id="5fe962c25933b99468a6f68b";
        this.workflow.push(act7);

        let act8 = new Activite();
        act8.title="Finalisation du contrat";
        act8.description="Finalisation du contrat";
        act8.executeur=CompteExecuteur.SERVICE_COMPTABLE.nom;
        act8.idExecuteur=CompteExecuteur.SERVICE_COMPTABLE.id;
        act8.formulaire="entente_directe/finalisation-contrat/finalisation-contrat.component";
        //act1.id="5fe962c25933b99468a6f68b";
        this.workflow.push(act8);

        console.log(this.workflow);

    }
    static fromObject(obj:any):ProcessusSelectionParEntenteDirect
    {
        let proc=new ProcessusSelectionParEntenteDirect(obj.title,obj.description,Client.fromObject(obj.client),obj.dateDebut,obj.dateFin);
<<<<<<< HEAD
        
=======
>>>>>>> e501e2fbb881b2b04c2567505af056554bfcd032
        proc.fromObject(obj);
        return proc
    }
    
}