import { EntityID } from './../../../models/EntityID';
export class Client
{
    id:String=(new EntityID()).toString();
    nomPrenom:String="";
    mail:String="";
    tel:String="";
    localisation:String="";

    constructor()
    {
        this.id=(new EntityID()).toString();
    }

    toString():any
    {
        return {
            id:this.id,
            nomPrenom:this.nomPrenom,
            mail:this.mail,
            tel:this.tel,
            localisation:this.localisation,
        };
    }

    static fromObject(obj:any):Client
    {
        let client:Client=new Client();
        client.id=obj["id"] || (new EntityID()).toString();
        client.nomPrenom=obj["nomPrenom"] || "";
        client.localisation=obj["localisation"]|| "";
        client.mail=obj["mail"]|| "";
        client.tel=obj["tel"]|| "";
        return client;
    }
}