import { Activite } from '../models/activite';
import { ProcessusType } from '../models/processustype';
import { Processus } from './../models/processus';
import { CompteExecuteur } from './compte_executeur';
import { Client } from './entites/client';
import { getDateNow } from './../../utils/date';

export class ProcessusSelectionParQualificationConsultant extends Processus
{
    constructor(title:String,description:String,client:Client,dateDebut,dateFin)
    {
        super();
        this.client=client;
        this.description=description;
        this.title=title;
        this.dateDebut=dateDebut;
        this.dateFin=dateFin;
        this.type=ProcessusType.SELECTION_PAR_QUALIFICATION;
        this.constructWorkflow();
    }

    constructWorkflow(): void {
    
        let act1 = new Activite();
        act1.title="Traitement du bon de commande";
        act1.description="Traitement du bon de commande du client dans le cas où AGRO-PME \
        est choisi par le client";
        act1.executeur=CompteExecuteur.SERVICE_COMPTABLE.nom;
        act1.idExecuteur=CompteExecuteur.SERVICE_COMPTABLE.id;
        act1.formulaire="qualification_consultant/bon_commande/bon_commande.component";
      
        this.workflow.push(act1);
    }
    static fromObject(obj:any):ProcessusSelectionParQualificationConsultant
    {
        let proc=new ProcessusSelectionParQualificationConsultant(obj.title,obj.description,Client.fromObject(obj.client),obj.dateDebut,obj.dateFin);
        proc.fromObject(obj);
        return proc
    }
    
}