import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ChatPagesComponent } from './pages/chat-pages/chat-pages.component';
import { FormCvAdminComponent } from './pages/form-cv-admin/form-cv-admin.component';
import { FormGrilleEvaluationCandidaturePersonnesComponent } from './pages/form-grille-evaluation-candidature-personnes/form-grille-evaluation-candidature-personnes.component';
import { FormIdentificationPersonnesRessourceComponent } from './pages/form-identification-personnes-ressource/form-identification-personnes-ressource.component';
import { HomeComponent } from './pages/home/home.component';
import { MailboxComposeComponent } from './pages/mailbox-compose/mailbox-compose.component';
import { MailboxInboxComponent } from './pages/mailbox-inbox/mailbox-inbox.component';
import { MailboxReadmailComponent } from './pages/mailbox-readmail/mailbox-readmail.component';
import { ProjetComponent } from './pages/projet/projet.component';
import { WebmailComponent } from './pages/webmail/webmail.component'
import { InformationGeneraleDossierComponent } from './pages/information-generale-dossier/information-generale-dossier.component';
import { FormFicheTravauxSuiviQuotidienComponent } from './pages/form-fiche-travaux-suivi-quotidien/form-fiche-travaux-suivi-quotidien.component';
import { FormFicheRapportPeiodiqueExecutionTravauxComponent } from './pages/form-fiche-rapport-peiodique-execution-travaux/form-fiche-rapport-peiodique-execution-travaux.component';
import { FormFicheTravauxRapportPeriodiqueComponent } from './pages/form-fiche-travaux-rapport-periodique/form-fiche-travaux-rapport-periodique.component';
import { FormFicheRapportMensuelVenteComponent } from './pages/form-fiche-rapport-mensuel-vente/form-fiche-rapport-mensuel-vente.component';
import { ListPersonnelComponent } from './pages/list-personnel/list-personnel.component';
import  { NewProjetComponent } from './../dashboard/pages/new-projet/new-projet.component';
import { FormulaireProcessusComponent } from './shared/formulaire-processus/formulaire-processus.component';
import { TousCouriersComponent } from './pages/tous-couriers/tous-couriers.component';
import { CouriersEntrantComponent } from './pages/couriers-entrant/couriers-entrant.component';
import { CouriersSortantComponent } from './pages/couriers-sortant/couriers-sortant.component';
import { ProcessHandlerComponent } from './pages/process-handler/process-handler.component';

const routes = [
    { 
        path: 'dashboard',
        /*canActivate: [GuardAuthService],*/
        component: DashboardComponent,
        children: [
            { path: '', redirectTo:'home',pathMatch: 'full'},
            { path:'home', component: HomeComponent,pathMatch: 'full' },
            { path:'personnels', component: ListPersonnelComponent,pathMatch: 'full' },
            { path:'form/cv/:id_personnel', component: FormCvAdminComponent},
            { path:'form/fiche/evaluation_candidature/:id_personnel', component: FormGrilleEvaluationCandidaturePersonnesComponent},
            { path:'form/fiche/identification_ressource', component: FormIdentificationPersonnesRessourceComponent,pathMatch: 'full' },
            { path:'test/ui/timeline', component: ProjetComponent,pathMatch: 'full' },
            { path:'mailbox/read', component: MailboxReadmailComponent,pathMatch: 'full' },
            { path:'mailbox/compose', component: MailboxComposeComponent,pathMatch: 'full' },
            { path:'mailbox/inbox', component: MailboxInboxComponent,pathMatch: 'full' },
            { path:'chats', component: ChatPagesComponent,pathMatch: 'full' },
            { path:'form/fiche/Information_générale_dossier', component: InformationGeneraleDossierComponent,pathMatch: 'full' },
            { path:'form/fiche/travaux_suivi_quotidien', component: FormFicheTravauxSuiviQuotidienComponent,pathMatch: 'full' },
            { path:'form/fiche/rapport_periodique_execution_travaux', component: FormFicheRapportPeiodiqueExecutionTravauxComponent,pathMatch: 'full' },
            { path:'form/fiche/travaux_rapport_periodique', component: FormFicheTravauxRapportPeriodiqueComponent,pathMatch: 'full' },
            { path:'form/fiche/rapport_mensuel_ventes', component: FormFicheRapportMensuelVenteComponent,pathMatch: 'full' },
            { path:'projet/new', component: NewProjetComponent,pathMatch: 'full' },
            { path:'projet/liste', component: ProjetComponent,pathMatch: 'full' },
            { path:'projet/test/processus', component: FormulaireProcessusComponent,pathMatch: 'full' },
            { path:'couriers/tous', component: TousCouriersComponent,pathMatch: 'full' },
            { path:'couriers/entrant', component: CouriersEntrantComponent,pathMatch: 'full' },
            { path:'couriers/sortant', component: CouriersSortantComponent,pathMatch: 'full' },
            { path:'projet/handler/:process_id', component: ProcessHandlerComponent},
            
        ]
    }    
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardModuleRouting {}