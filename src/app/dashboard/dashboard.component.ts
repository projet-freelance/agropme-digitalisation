import { Component, ElementRef, OnInit } from '@angular/core';
import { FirebaseApi } from '../services/firebase/FirebaseApi';




@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  showFlashMessage=false;
  messageFlashMessage:String="";
  typeFlashMessage:String="";
  durationFlashMessage=2000;
  temporairyFlash:boolean=false;

  constructor(private fetchApi:FirebaseApi) { }
  
  ngOnInit(): void {
    //this.fetchApi.handleConnexionState((state)=>this.handleStateConnexion(state))
  }
  changeFlashMessageStatut(data)
  {
    this.showFlashMessage=data.show;
  }
  handleStateConnexion(state)
  {
    this.showFlashMessage =false;

    if(state.connected)
    {
      this.showFlashMessage = true;
      this.typeFlashMessage = "success";
      this.messageFlashMessage="Connexion établie";
      setTimeout(() => this.showFlashMessage = false,this.durationFlashMessage);  
    }
    else
    {
      this.showFlashMessage = true;
      this.typeFlashMessage = "danger";
      this.messageFlashMessage="Connexion perdue"
    }
  }

}
