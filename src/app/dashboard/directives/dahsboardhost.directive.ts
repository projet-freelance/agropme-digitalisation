import {Directive, ViewContainerRef } from '@angular/core';

@Directive({
    'selector':'[dashboard_host]'
})
export class DashboardDirective
{
    constructor(public viewContainerRef: ViewContainerRef) { }
}
