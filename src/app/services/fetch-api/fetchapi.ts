import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
//import * as firebase from 'fir
import { ResultStatut } from './resultstatut';

@Injectable({
  providedIn: 'root'
})

export class FetchApi {

  static fetchConfig:any = {
    apiKey: "AIzaSyBzip1xgUIs51_zKy6Yw87lIfmVKHaaBiA",
    authDomain: "ya-ndap.firebaseapp.com",
    databaseURL: "https://ya-ndap.firebaseio.com",
    projectId: "ya-ndap",
    storageBucket: "ya-ndap.appspot.com",
    messagingSenderId: "392743949439",
    appId: "1:392743949439:web:110d572ef9d0ef61775b4a",
    measurementId: "G-W0GJEMR2YM"
  };
  debug:boolean=false;
  offlineMode:boolean=false;
  db:any;

  constructor() {

    // Initialize Firebase
    firebase.initializeApp(FetchApi.fetchConfig);
    this.db=firebase.database();
    this.setDebugMode();
    this.setModeApp();
  }
  setDebugMode()
  {
    // if(this.debug) firebase.firestore.setLogLevel('debug');

  }
  setModeApp()
  {
    // if(this.offlineMode) firebase.firestore().enablePersistence();
  }
  add(url:string,value:any):Promise<ResultStatut>
  {
    let action=new ResultStatut();
    return new Promise((resolve, reject)=>{
      this.db.ref(url).push().set(value).then((doc)=>{
        action.description="successful add new collection";
        resolve(action);
      }).catch((err)=>{
        action.apiCode=err.code;
        action.code=ResultStatut.UNKNOW_ERROR;
        action.message="error";
        action.description="Description of error: "+err;
        reject(action);
      });
    });
  }
  set(url:string,value:any):Promise<ResultStatut>
  {

    let action=new ResultStatut();
    return new Promise<ResultStatut>((resolve,reject)=>{
      this.db.ref(url).set(value).then(()=>{
        action.message="success";
        action.description="successful set new collection";
        resolve(action);
      }).catch((err)=>{
        action.apiCode=err.code;
        action.code=ResultStatut.UNKNOW_ERROR;
        action.message="error";
        action.description="Description of error: "+err;
        reject(action)
      });
    })
  }
  fetchOnce(url:string):Promise<ResultStatut>
  {
    let action=new ResultStatut();
    return new Promise((resolve,reject)=>{
      this.db.ref(url).once('value')
      .then( (doc)=>{
        try
        {
          action.result={};
          doc.forEach(element => {
            action.result[element.key]=element.val();
          });
          action.description="Successful fetching information";
          resolve(action);
        }
        catch (err) {
          action.apiCode=err.code;
          action.code=ResultStatut.UNKNOW_ERROR;
          action.message="error";
          action.description=`Description of error: ${err}`;
          reject(action);
        }
      })
    });
  }


  fetch(url:string):Promise<ResultStatut>
  {
    let action=new ResultStatut();
    return new Promise<ResultStatut>((resolve,reject)=>{
      this.db.ref(url).on('value', (doc)=>{
        try
        {
          let r=[];
          doc.forEach(element => {
            r.push(element.val());
          });
          action.description="Successful fetching information";
          action.result=r;
          resolve(action);
        }
        catch (err) {
          action.apiCode=err.code;
          action.code=ResultStatut.UNKNOW_ERROR;
          action.message="error";
          action.description=`Description of error: ${err}`;
          reject(action);
        }
      });
    });
  }

  update(url:string,updates:any):Promise<ResultStatut>
  {
    let action=new ResultStatut();
    return new Promise<ResultStatut>((resolve,reject)=>{
      try
      {
        this.db.ref(url).update(updates);
        action.description="Successful update information";
        resolve(action);
      }
      catch(err)
      {
        action.apiCode=err.code;
        action.code=ResultStatut.UNKNOW_ERROR;
        action.message="error";
        action.description=`Description of error: ${err}`;
        reject(action);
      }
    });
  }
  delete(url:string):Promise<ResultStatut>
  {
    let action=new ResultStatut();
    return new Promise<ResultStatut>((resolve,reject)=>{
      try
      {
        this.db.ref(url).remove();
        action.description="Successful deleting information";
        resolve(action);
      }
      catch(err)
      {
        action.apiCode=err.code;
        action.code=ResultStatut.UNKNOW_ERROR;
        action.message="error";
        action.description=`Description of error: ${err}`;
        reject(action);
      }
    });

  }
  get user()
  {
    return firebase.auth().currentUser;
  }

  signInApi(email:string,password:string): Promise<ResultStatut>
  {
    let result:ResultStatut=new ResultStatut();
    return new Promise(async (resolve,reject)=>{
        firebase.auth().signInWithEmailAndPassword(email,password)
        .then(()=>{
          result.description="Authentification successful";
          resolve(result);
        })
        .catch((error)=>
        {
          result.code=ResultStatut.UNKNOW_ERROR;
          result.apiCode=error.code;
          result.message="error";
          result.description=`Description of error: ${error}`;
          reject(result);
        })
    });
  }
  signOutApi()
  {
    firebase.auth().signOut();
  }
  createUserApi(email:string,password:string):Promise<ResultStatut>
  {
    let result:ResultStatut=new ResultStatut();
    return new Promise( async (resolve,reject)=>{
        firebase.auth().createUserWithEmailAndPassword(email,password)
        .then(()=>{
          result.description="Account was created successful";
          resolve(result);
        })
        .catch( (error)=>{
        result.code=ResultStatut.UNKNOW_ERROR;
        result.apiCode=error.code;
        result.message=`error: ${error.code}`;
        result.description=`Description of error: ${error.message}`;
        reject(result);
      });
    });
  }
}
