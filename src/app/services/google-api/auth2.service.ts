class GoogleAuth2 {
    public static google_key_api="google_key_api";

    private gapiSetup:Boolean=false;
    private authInstance=null;

   initGoogleAuth():Promise<void>
   { 
       const pload = new Promise((resolve)=>
       { 
           gapi.load('auth2',resolve)
       });

       return pload.then(()=>gapi.auth2.init({client_id:GoogleAuth2.google_key_api}))
                    .then((auth)=>{
                        this.gapiSetup = true;
                        this.authInstance = auth;
                    })
   }

   async chechIfUserAuthentificated():Promise<boolean>
   {
       if(this.gapiSetup)
       {
           await this.initGoogleAuth();
       }
       return this.authInstance.isSignedIn.get();
   }
}