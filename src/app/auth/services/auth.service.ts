import { Injectable, OnDestroy } from '@angular/core';

import { ResultStatut } from './../../services/firebase/ResultStatut';
import { User } from './../../models/user';
import { FirebaseApi } from './../../services/firebase/FirebaseApi';
import { FirebaseConstant } from './../../services/firebase/FirebaseConstant'

@Injectable({
  providedIn:'root'
})
export class AuthService {

  user:User =null;
  isAuth:boolean=false;
  constructor(private fetchApi:FirebaseApi) {}
  
  signIn(userN:User):Promise<ResultStatut>
  {
    let action=new ResultStatut();
    return new Promise<ResultStatut>((resolve,reject)=>{
      this.fetchApi.signInApi(userN.email.toString(),userN.password.toString())
      .then(result=>{
        this.isAuth=true;
        action=result;
        this.user=new User();
        this.user.hydrate({
          email:this.fetchApi.user.email,
          name:this.fetchApi.user.displayName,
          phone:this.fetchApi.user.phoneNumber,
          profile:this.fetchApi.user.photoURL,
          id:this.fetchApi.user.uid,
        });
        resolve(result);
      })
      .catch(result=>{
        this.fetchApi.handleApiError(result);
        reject(result);
      })
    });
  }

  signOut():void
  {
    this.user=null;
    this.isAuth=false;
    this.fetchApi.signOutApi();
  }
  isConnected():boolean
  {
    // this.getUserDataFromStorage();
    return this.isAuth && this.user!=null && this.user!=undefined;
  }
  getUser():User
  {
    return this.user;
  }
  signInNewUser(user:User):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>
    {
      this.fetchApi.createUserApi(user.email.toString(),user.password.toString())
      .then((result)=>{
        return this.signIn(user);
      })
      .catch(e=>
      {
        this.fetchApi.handleApiError(e);
        reject(e)
      })
    });
  }
  /*private getUserDataFromStorage():void
  {
    if(this.isAuth || !keyExist("is_auth")) return;
    this.isAuth=deSerializeData("is_auth")==null?false:true;
    this.user=User.fromObject(deSerializeData("current_user"));

  }
  private setUserDataToStorage():void
  {
    console.log("User",this.isAuth,this.user);
    if(!this.isAuth) return;
    serializeData("is_auth",this.isAuth);
    serializeData("current_user",this.user);
  }*/
}
