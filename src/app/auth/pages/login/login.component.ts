import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from './../../services/auth.service';
import { User } from './../../../models/user';
import { ResultStatut } from 'src/app/services/firebase/ResultStatut';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form:FormGroup;
  submittedForm:boolean=false;
  waitingForm:boolean=false;
  successfulAuthentification=false;
  errorFormMessage=""

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.form=new FormGroup({
      email:new FormControl('',[Validators.required,Validators.email]),
      password: new FormControl('',[Validators.required])
    });
  }
  submit()
  {
    if(!this.form.valid)
    {
      this.errorFormMessage="Formulaire invalide";
      this.submittedForm = true;
      return;
    }
    this.submittedForm=true;
    this.waitingForm=true;
    this.authService.signIn(new User(this.form.value.email,this.form.value.password))
    .then((result:ResultStatut)=>{
      this.waitingForm = false;
      this.successfulAuthentification = true;
      setTimeout(() => {
        this.router.navigate(["dashboard"]);
      }, 2000);
    })
    .catch((error:ResultStatut)=>{
        this.errorFormMessage=error.message;
        this.waitingForm = false;
        this.successfulAuthentification = false;
    });
  }

}
