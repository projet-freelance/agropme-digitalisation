import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';

const routes = [
  { 
      path: 'login',
      /*canActivate: [GuardAuthService],*/
      component: LoginComponent,
      pathMatch:"full"
  }    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}


